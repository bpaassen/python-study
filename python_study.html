<!DOCTYPE html>
<!--
Python Data Science Programming Learning Environment

Copyright (C) 2019
Benjamin Paaßen
AG Machine Learning
Bielefeld University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
-->

<!--
This file implements a very simple learning environment for data science
programming in Python. Students are asked to implement a gradient descent
algorithm to optimize a one-dimensional polynomial function.

While programming, the environment records all intermediate states of the
student's solution, thus providing an impression of how students tend to
solve this kind of task. The progress of students is checked against unit
tests.

Note that this environment relies on two dependencies, namely pyodide
to run Python code in the browser and the ace source code editor.
-->
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Gradient Descent Task</title>
<!--
    <link rel="stylesheet" href="css/main.css" />
    <link rel="icon" href="images/favicon.png" />-->
    <script src="pyodide-0.14-small/pyodide.js"  type="text/javascript"></script>
    <script>
    // load pyodide in a promise
    languagePluginLoader.then(() => {
        // pyodide is now ready to use...
        console.log('Successfully loaded pyodide with Python version ' + pyodide.runPython('import sys\nsys.version'));

        // load numpy and matplotlib
        pyodide.loadPackage(['numpy', 'matplotlib']).then(() => {
          document.getElementById("environment").style.display = 'block';
          document.getElementById("loading").style.display = 'none';
        });
    });

    function start() {
        document.getElementById("task").style.display = 'block';
        document.getElementById("study-info").style.display = 'none';
    }

    var error_message_regexp = /File \"\<exec\>\", line (\d+), in (f|\<module\>)/

    function handle_error(err) {
        var message = 'Whoops, the compiler did not understand your code. ';
        // pre-process the error message to cut off unhelpful metadata
        var m = err.message.match(error_message_regexp);
        if(m) {
            var n = err.message.search(error_message_regexp);
            message += 'It found the following error in line ' + m[1] + ': ' + err.message.substring(n + m[0].length);
        } else {
            message += 'Here\'s the compiler error message: ' + err.message;
        }
        // return the message
        return message;
    }

    function check_fun() {
        // check the function definition with a unit test
        var editor = ace.edit("editor-fun");
        var code = editor.getValue();
        // simply append the unit test to the python code
        code += `
x = [-2., -1., -0.5, 0., 0.5, 0.8, 1., 2.]
y = [11.5, -0.25, -0.3125, 0., -0.0625, -0.0304, 0.25, 12.5]
if not isinstance(f(0), float):
    message = "Not quite there yet: The output of f should be a floating point number."
else: 
    message = "Great work! Let's proceed to the next task."
    for i in range(len(x)):
        if abs(f(x[i]) - y[i]) > 1E-3:
            message = 'The function definition was not quite right: Expected output %g for input %g but got %g instead' % (y[i], x[i], f(x[i]))
            break
message`
        // try to run the student code with the unit test
        var message = '';
        try {
            message = pyodide.runPython(code);
        } catch(err) {
            message = handle_error(err);
        }
        // show the response message
        document.getElementById("feedback").textContent = message;
        // call the log function
        log_code('fun');
        // enable the next task if this task was fulfilled
        if(message.startsWith('Great work!')) {
            document.getElementById("task-plt").style.display = 'block';
        }
    }

    function check_plt() {
        // check the plotting task with a unit test
        var editor = ace.edit("editor-plt");
        var code = editor.getValue();
        // simply append the unit test to the python code
        code += `
if not isinstance(x, np.ndarray):
    message = 'x is not a numpy array'
elif np.abs(x[0] + 1.5) > 1E-3:
    message = 'The first element of x should be -1.5 but was %g' % x[0]
elif len(x) != 101:
    message = 'x does not yet have the right length; got: %s' % str(x)
elif np.any(np.abs(x[1:] - x[:-1] - 0.03) > 1E-3):
    message = 'The step size in x should be 0.03 but got: %s' % str(x)
elif np.any(np.abs(y - f(x)) > 1E-3):
    message = 'y should be equal to f(x)'
elif not plt.get_fignums():
    message = 'x and y seem right, but there is no plot yet.'
else:
    import io, base64
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    message = 'data:image/png;base64,' + base64.b64encode(buf.read()).decode('UTF-8')
message`;
        // try to run the student code with the unit test
        var message = '';
        try {
            message = pyodide.runPython(code);
        } catch(err) {
            message = handle_error(err);
        }
        // show the response message or plot it if it is the image
        if(message.startsWith('data:image/png;base64,')) {
            document.getElementById("pyplotfigure").src=message;
            document.getElementById("pyplotfigure2").src=message;
            document.getElementById("feedback").textContent = 'Great work! Your plot seems right. Let\'s proceed to the next task.';
            document.getElementById("task-grad").style.display = 'block';
        } else {
            document.getElementById("feedback").textContent = message;
        }
        // call the log function
        log_code('plt');
    }

    function check_grad() {
        // check the gradient task with a unit test
        var editor = ace.edit("editor-grad");
        var code = editor.getValue();
        // simply append the unit test to the python code
        code += `
x = [-2., -1., -0.5, 0., 0.5, 0.8, 1., 2.]
y = [-27.75, -1.75, 0.75, 0.25, -0.25, 0.698, 2.25, 28.25]
if not isinstance(f_grad(0), float):
    message = "The output of f_grad should be a number."
else: 
    message = "Great work! Your gradient function seems right. Let's proceed to the next task."
    for i in range(len(x)):
        if abs(f_grad(x[i]) - y[i]) > 1E-3:
            message = 'The gradient definition was not quite right; expected gradient %g for input %g but got %g instead' % (y[i], x[i], f_grad(x[i]))
            break
message`
        // try to run the student code with the unit test
        var message = '';
        try {
            message = pyodide.runPython(code);
        } catch(err) {
            message = handle_error(err);
        }
        // show the feedback message to the student
        document.getElementById("feedback").textContent = message;
        // call the log function
        log_code('grad');
        // enable the next task if this task was fulfilled
        if(message.startsWith('Great work!')) {
            document.getElementById("task-desc").style.display = 'block';
        }
    }

    var while_pattern = /(\s*)while (.*):[\r\n](\s*)/m

    function check_desc() {
        // retrieve the function and gradient definitions
        // from before
        var fun_code  = ace.edit("editor-fun").getValue();
        var grad_code = ace.edit("editor-grad").getValue();

        // get the student's gradient descent code
        var editor = ace.edit("editor-desc");
        var desc_code = editor.getValue();
        // pre-process the code to prevent some notorious cases of
        // endless loops which could break down our system
        var m = desc_code.match(while_pattern);
        if(m) {
            var out_tabs = m[1];
            var condition = m[2];
            var in_tabs = m[3];
            var safe_loop = out_tabs + 'counter = 0\n' + out_tabs + 'while ' + condition + ':\n' + in_tabs + 'if counter > 1000:\n' + in_tabs + '  break\n' + in_tabs + 'else:\n' + in_tabs + '  counter += 1\n' + in_tabs;
            desc_code = desc_code.replace(m[0], safe_loop);
        }
        // then, concatenate with the function and gradient code
        var code = fun_code + '\n' + grad_code + '\n' + desc_code;
        // append the unit test to the python code
        code += `
x0 = 0.6
eta = 0.1
xfin = gradient_descent(f_grad, x0, eta)
if not isinstance(xfin, float):
    message = "The output of gradient descent should be a single floating point number"
elif np.abs(xfin - 0.63325) > 1E-3:
    message = "Your gradient descent does not seem to converge to the correct point. Could you check again?"
else: 
    message = "Great work! Your gradient descent function seems right. Let's proceed to the next task."
message`
        // try to run the student code with the unit test
        var message = '';
        try {
            message = pyodide.runPython(code);
        } catch(err) {
            message = handle_error(err);
        }
        // show the feedback message to the student
        document.getElementById("feedback").textContent = message;
        // call the log function
        log_code('desc');
        // enable the next task if this task was fulfilled
        if(message.startsWith('Great work!')) {
            document.getElementById("task-fin").style.display = 'block';
        }
    }

    function check_fin() {
        // retrieve the function, gradient, and gradient descent
        // definitions from before
        var fun_code  = ace.edit("editor-fun").getValue();
        var grad_code = ace.edit("editor-grad").getValue();
        var desc_code = ace.edit("editor-desc").getValue();
        // pre-process the code to prevent some notorious cases of
        // endless loops which could break down our system
        var m = desc_code.match(while_pattern);
        if(m) {
            var out_tabs = m[1];
            var condition = m[2];
            var in_tabs = m[3];
            var safe_loop = out_tabs + 'counter = 0\n' + out_tabs + 'while ' + condition + ':\n' + in_tabs + 'if counter > 1000:\n' + in_tabs + '  break\n' + in_tabs + 'else:\n' + in_tabs + '  counter += 1\n' + in_tabs;
            desc_code = desc_code.replace(m[0], safe_loop);
        }
        // check the final task with a unit test
        var editor = ace.edit("editor-fin");
        var code = fun_code + '\n' + grad_code + '\n' + desc_code + '\n' + editor.getValue();
        // simply append the unit test to the python code
        code += `
if f(x) + 0.434 > 1E-3:
    message = 'Your final function value is not good enough; the best value is expected to be about -0.434'
else:
    message = 'Great work! You completed all tasks!'
message`;
        // try to run the student code with the unit test
        var message = '';
        try {
            message = pyodide.runPython(code);
        } catch(err) {
            message = handle_error(err);
        }
        // show the feedback message to the student
        document.getElementById("feedback").textContent = message;
        // call the log function
        log_code('fin');
        // enable the next task if this task was fulfilled
        if(message.startsWith('Great work!')) {
            document.getElementById("complete-button-div").style.display = 'block';
        }
    }

    // Dictionary to store timeouts for each editor
    var editor_timeouts = {};
    // Dictionary to store the logged code for each editor
    var editor_logs = {}

    function log_code(id) {

        // clear the current timeout if there is one stored
        if(id in editor_timeouts) {
            clearTimeout(editor_timeouts[id]);
        }

        // trigger a delayed action; if there are no other change events
        // for two seconds, we log the code
        editor_timeouts[id] = setTimeout(function() {
            var editor = ace.edit("editor-" + id);
            var code = editor.getValue();
            // initialize a new log list for this editor if none exists yet
            if(!(id in editor_logs)) {
                editor_logs[id] = [];
            }
            // push the code onto the log
            editor_logs[id].push(code);
        }, 2000);
    }

    function final_page() {
        document.getElementById("task").style.display = 'none';
        document.getElementById("study-debrief").style.display = 'block';
        var code = ace.edit("editor-fun").getValue() + '\n';
        code    += ace.edit("editor-plt").getValue() + '\n';
        code    += ace.edit("editor-grad").getValue() + '\n';
        code    += ace.edit("editor-desc").getValue() + '\n';
        code    += ace.edit("editor-fin").getValue();
        var editor = ace.edit("editor-summary");
        editor.setValue(code);
        editor.clearSelection();
    }

    function generate_data() {
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.style = "display: none";
        var data = JSON.stringify(editor_logs, null, 2);
        data = new Blob([data], {type: "octet/stream"});
        data = window.URL.createObjectURL(data);
        a.href = data;
        a.download = 'study_log_data.json';
        a.click();
        window.URL.revokeObjectURL(data);
    }

    </script>
    <style type="text/css" media="screen">
        #container {
            background-image: url(background.svg);
            width: 1920px;
            height: 300px;
            font-size: 22px;
            font-family: Arial, sans-serif;
        }

        #study-info {
            left: 200px;
            width: 600px;
            text-align: justify;
            position: absolute;
            top: 20px;
        }

        #environment, #study-debrief {
            left: 200px;
            width: 1250px;
            position: absolute;
            top: 20px;
        }

        #editor-container {
            float: left;
            width: 600px;
        }

        .editor { 
            width: 600px;
            height: 100px;
        }

        #editor-plt {
            height: 150px;
        }

        #editor-desc {
            height: 200px;
        }

        #editor-summary {
            height: 600px;
        }

        #feedback-column {
            float: right;
            width: 600px;
            height: 3000px;
            padding-top: 120px;
        }

        #feedback-container {
            position: sticky;
            top: 10px;
        }

        #study-debrief-left {
            float: left;
            width: 600px;
        }

        #study-debrief-right {
            float: right;
            width: 600px;
            padding-top: 120px;
        }

        .button {
            color: black;
            background-color: white;
            font-size: 22px;
        }
    </style>
  </head>
  <body>
    <div id="container">
    <div id="study-info">
    <h2>Python Data Science Programming Study</h2>

    <p>Welcome to this learning environment for data science programming
    in Python. This learning environment is part of an ongoing study.
    In this study, we are interested in how students solve a Python
    programming task.</p>

    <p>We will ask you to write a short Python program and will record how
    your source code develops over time. The data stays on your computer until
    the end of the study, when we will ask you to send the data to us, which will then
    be made accessible to the research community for further study.
    If you do not feel comfortable with that, you can abort the study at
    any time and no data will be stored.</p>

    <p>This study will take about 30 minutes. We do not expect any negative
    effects. Still, you can abort the study at any time.</p>

    <p>If you have questions regarding the study, you can ask
    <a href="mailto:bpaassen@techfak.uni-bielefeld.de">the principle investigator</a>
    via e-mail before proceeding with the study.</p>

    <p>By clicking 'I accept', you confirm that you have read and understood
    this text and consent to the conditions laid out above.</p>

    <input class="button" type="button" onclick="start()" value="I accept"/>
    </div>

    <div id="task" style="display: none">
    <div id="loading">Loading the programming environment ...</div>
    <div id="environment" style="display: none">
        <div id="editor-container">
        <script src="ace-small/ace.js" type="text/javascript" charset="utf-8"></script>
        <h3>Programming tasks</h3>
        <!-- Task 1: Function definition -->
        <p>Our goal in this programming exercise is to find the global
        minimum of the function f(x) = x<sup>4</sup> - x<sup>2</sup> + 0.25 x.
        </p>

        <p>Your first task is to define the function f in Python.</p>
        <div id="editor-fun" class="editor">def f(x):
    return "Write the Python function expression here"
        </div>
        <script>
            var editor = ace.edit("editor-fun");
            editor.setFontSize(18);
            editor.setTheme("ace/theme/twilight");
            editor.session.setMode("ace/mode/python");
            editor.session.on('change', function(delta) { log_code('fun'); });
        </script>
        <p>Please press on the 'Check Code' button to get feedback on your code.
        The feedback will be displayed on the right.
        Once your code seems correct, you can proceed with the next task.</p>
        <input class="button" type="button" onclick="check_fun()" value="Check Code"/>
        <!-- Task 2: Function Plotting-->
        <div id="task-plt" style="display: none">
        <p>Next, your task is to plot the function using matplotlib.
        To that end, use numpy to generate the array [-1.5, -1.47, ..., 1.47, 1.5]
        (for example using the functions
        <a href="https://docs.scipy.org/doc/numpy/reference/generated/numpy.arange.html" target="_blank">arange</a>
        or <a href="https://docs.scipy.org/doc/numpy/reference/generated/numpy.linspace.html" target="_blank">linspace</a>),
        then use your function f to compute the corresponding y
        values, and finally use matplotlib to
        <a href="https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.plot.html" target="_blank">plot</a>
        the data.</p>
        <div id="editor-plt" class="editor">import numpy as np
x = "generate the array [-1.5, -1.47, ..., 1.47, 1.5] here"
y = f(x)
import matplotlib.pyplot as plt
"generate a plot of y versus x here"
        </div>
        <script>
            var editor = ace.edit("editor-plt");
            editor.setFontSize(18);
            editor.setTheme("ace/theme/twilight");
            editor.session.setMode("ace/mode/python");
            editor.session.on('change', function(delta) { log_code('plt'); });
        </script>
        <p>Be advised that 'Check Code' may take a few seconds here because
        starting up matplotlib takes some time.</p>
        <input class="button" type="button" onclick="check_plt()" value="Check Code"/>
        </div>
        <!-- Task 3: Gradient Definition -->
        <div id="task-grad" style="display: none">
        <p>Next, your task is to write a function that computes the <em>gradient</em>
        of f.</p>
        <div id="editor-grad" class="editor">def f_grad(x):
    return "compute the gradient of f at point x here"
        </div>
        <script>
            var editor = ace.edit("editor-grad");
            editor.setFontSize(18);
            editor.setTheme("ace/theme/twilight");
            editor.session.setMode("ace/mode/python");
            editor.session.on('change', function(delta) { log_code('grad'); });
        </script>
        <input class="button" type="button" onclick="check_grad()" value="Check Code"/>
        </div>
        <!-- Task 4: Gradient Descent Definition -->
        <div id="task-desc" style="display: none">
        <p>Now comes the big step: Your task is to write a function gradient_descent
        that implements the <a href="https://en.wikipedia.org/wiki/Gradient_descent#Description" target="_blank">gradient descent algorithm</a>.
        More specifically, your function should take as inputs a gradient
        function f_grad, a starting point x0, and a step size eta.</p>

        <p>Then, your function should perform gradient steps until the gradient
        absolute value is less than 0.001.</p>

        <p>Finally, the output of your function should be the final value x.</p>
        <div id="editor-desc" class="editor">def gradient_descent(f_grad, x0, eta):
    x = x0
    # TODO implement gradient descent here. Beware of endless loops!
    return x
        </div>
        <script>
            var editor = ace.edit("editor-desc");
            editor.setFontSize(18);
            editor.setTheme("ace/theme/twilight");
            editor.session.setMode("ace/mode/python");
            editor.session.on('change', function(delta) { log_code('desc'); });
        </script>
        <input class="button" type="button" onclick="check_desc()" value="Check Code"/>
        </div>
        <!-- Task 5: Gradient Descent Application -->
        <div id="task-fin" style="display: none">
        <p>Finally, select a starting point x0 and a step size eta such that
        your gradient descent finds the global optimum.</p>
        <div id="editor-fin" class="editor">x0 = 1. # Change this value
eta = 1. # Change this value
x = gradient_descent(f_grad, x0, eta)
        </div>
        <script>
            var editor = ace.edit("editor-fin");
            editor.setFontSize(18);
            editor.setTheme("ace/theme/twilight");
            editor.session.setMode("ace/mode/python");
            editor.session.on('change', function(delta) { log_code('fin'); });
        </script>
        <input class="button" type="button" onclick="check_fin()" value="Check Code"/>
        </div>
        <div id="complete-button-div" style="display: none">
        <input class="button" type="button" onclick="final_page()" value="Go to final page"/>
        </div>
        </div>
        <!-- Container for output and plotting -->
        <div id="feedback-column">
        <div id="feedback-container">
        <h3>Feedback and Output</h3>
        <img id="pyplotfigure"/>
        <p id="feedback"></p>
        </div>
        </div>
    </div>
    </div>
    <div id="study-debrief" style="display: none">
    <div id="study-debrief-left">
    <h2>Debriefing</h2>

    <p>Thank you for completing this programming task.</p>

    <p>If you click on 'Generate data file', you can generate a
    file that contains all your programming data. We ask you to
    kindly send that file to us via e-mail to <a href="mailto:bpaassen@techfak.uni-bielefeld.de">bpaassen@techfak.uni-bielefeld.de</a>.
    You can also inspect the file beforehand to see exactly
    which data is transmitted to us. <strong>Except for your e-mail,
    no data is transmitted to us</strong>.</p>

    <p>As soon as we have stored your anonymous data file, we will
    delete your e-mail. No personalized data will be stored.</p>

    <input class="button" type="button" onclick="generate_data()" value="Generate data file"/>

    <p>Thank you again for taking part in our study!</p>
    </div>
    <div id="study-debrief-right">
    <p>Your final solution was: </p>

    <div id="editor-summary" class="editor">
    </div>
    <script>
        var editor = ace.edit("editor-summary");
        editor.setFontSize(18);
        editor.setTheme("ace/theme/twilight");
        editor.session.setMode("ace/mode/python");
    </script>

    <p>Your function plot was:</p>

    <img id="pyplotfigure2"/>
    </div>
    </div>
    </div>
  </body>
</html>
