# Generated from MiniPython3.g4 by ANTLR 4.8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


from antlr4.Token import CommonToken
import re
import importlib

# Allow languages to extend the lexer and parser, by loading the parser dynamically
module_path = __name__[:-5]
language_name = __name__.split('.')[-1]
language_name = language_name[:-5]  # Remove Lexer from name
LanguageParser = getattr(importlib.import_module('{}Parser'.format(module_path)), '{}Parser'.format(language_name))



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2E")
        buf.write("\u02b0\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:")
        buf.write("\4;\t;\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\t")
        buf.write("C\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I\tI\4J\tJ\4K\tK\4L\t")
        buf.write("L\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT\4U\t")
        buf.write("U\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4")
        buf.write("^\t^\4_\t_\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6")
        buf.write("\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3")
        buf.write("\n\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r\3\r\3\16")
        buf.write("\3\16\3\16\3\17\3\17\3\17\3\20\3\20\3\20\3\21\3\21\3\21")
        buf.write("\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\24\3\24\3\24")
        buf.write("\3\24\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\27\3\27")
        buf.write("\3\27\3\30\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\31\3\32")
        buf.write("\3\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\34")
        buf.write("\3\34\3\34\3\34\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\35")
        buf.write("\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\37")
        buf.write("\3\37\3\37\3\37\3\37\3 \3 \3!\3!\3!\3!\3\"\3\"\3#\3#\3")
        buf.write("#\3$\3$\3$\3%\3%\3&\3&\3\'\3\'\3(\3(\3)\3)\3*\3*\3*\3")
        buf.write("+\3+\3+\3,\3,\3,\3-\3-\3-\3.\3.\3.\3/\3/\3/\3/\3\60\3")
        buf.write("\60\3\60\3\61\3\61\3\62\3\62\3\63\3\63\3\63\3\63\3\63")
        buf.write("\3\64\3\64\3\64\3\64\3\64\3\64\3\65\3\65\3\65\3\65\3\65")
        buf.write("\3\66\3\66\5\66\u017a\n\66\3\67\3\67\3\67\5\67\u017f\n")
        buf.write("\67\38\38\38\38\58\u0185\n8\39\39\39\59\u018a\n9\39\3")
        buf.write("9\59\u018e\n9\39\59\u0191\n9\59\u0193\n9\39\39\3:\3:\7")
        buf.write(":\u0199\n:\f:\16:\u019c\13:\3;\3;\3;\3;\3;\5;\u01a3\n")
        buf.write(";\3;\3;\5;\u01a7\n;\3<\3<\3<\3<\3<\5<\u01ae\n<\3<\3<\5")
        buf.write("<\u01b2\n<\3=\3=\7=\u01b6\n=\f=\16=\u01b9\13=\3=\6=\u01bc")
        buf.write("\n=\r=\16=\u01bd\5=\u01c0\n=\3>\3>\3>\6>\u01c5\n>\r>\16")
        buf.write(">\u01c6\3?\3?\3?\6?\u01cc\n?\r?\16?\u01cd\3@\3@\3@\6@")
        buf.write("\u01d3\n@\r@\16@\u01d4\3A\3A\5A\u01d9\nA\3B\3B\5B\u01dd")
        buf.write("\nB\3B\3B\3C\3C\3C\5C\u01e4\nC\3C\3C\3D\3D\3E\3E\3E\7")
        buf.write("E\u01ed\nE\fE\16E\u01f0\13E\3E\3E\3E\3E\7E\u01f6\nE\f")
        buf.write("E\16E\u01f9\13E\3E\5E\u01fc\nE\3F\3F\3F\3F\3F\7F\u0203")
        buf.write("\nF\fF\16F\u0206\13F\3F\3F\3F\3F\3F\3F\3F\3F\7F\u0210")
        buf.write("\nF\fF\16F\u0213\13F\3F\3F\3F\5F\u0218\nF\3G\3G\5G\u021c")
        buf.write("\nG\3H\3H\3I\3I\3I\3I\5I\u0224\nI\3J\3J\3K\3K\3L\3L\3")
        buf.write("M\3M\3N\3N\3O\5O\u0231\nO\3O\3O\3O\3O\5O\u0237\nO\3P\3")
        buf.write("P\5P\u023b\nP\3P\3P\3Q\6Q\u0240\nQ\rQ\16Q\u0241\3R\3R")
        buf.write("\6R\u0246\nR\rR\16R\u0247\3S\3S\5S\u024c\nS\3S\6S\u024f")
        buf.write("\nS\rS\16S\u0250\3T\3T\3T\7T\u0256\nT\fT\16T\u0259\13")
        buf.write("T\3T\3T\3T\3T\7T\u025f\nT\fT\16T\u0262\13T\3T\5T\u0265")
        buf.write("\nT\3U\3U\3U\3U\3U\7U\u026c\nU\fU\16U\u026f\13U\3U\3U")
        buf.write("\3U\3U\3U\3U\3U\3U\7U\u0279\nU\fU\16U\u027c\13U\3U\3U")
        buf.write("\3U\5U\u0281\nU\3V\3V\5V\u0285\nV\3W\5W\u0288\nW\3X\5")
        buf.write("X\u028b\nX\3Y\5Y\u028e\nY\3Z\3Z\3Z\3[\6[\u0294\n[\r[\16")
        buf.write("[\u0295\3\\\3\\\7\\\u029a\n\\\f\\\16\\\u029d\13\\\3]\3")
        buf.write("]\5]\u02a1\n]\3]\5]\u02a4\n]\3]\3]\5]\u02a8\n]\3^\5^\u02ab")
        buf.write("\n^\3_\3_\5_\u02af\n_\6\u0204\u0211\u026d\u027a\2`\3\3")
        buf.write("\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16")
        buf.write("\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61")
        buf.write("\32\63\33\65\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*")
        buf.write("S+U,W-Y.[/]\60_\61a\62c\63e\64g\65i\66k\67m8o9q:s;u<w")
        buf.write("=y>{?}@\177A\u0081B\u0083C\u0085D\u0087E\u0089\2\u008b")
        buf.write("\2\u008d\2\u008f\2\u0091\2\u0093\2\u0095\2\u0097\2\u0099")
        buf.write("\2\u009b\2\u009d\2\u009f\2\u00a1\2\u00a3\2\u00a5\2\u00a7")
        buf.write("\2\u00a9\2\u00ab\2\u00ad\2\u00af\2\u00b1\2\u00b3\2\u00b5")
        buf.write("\2\u00b7\2\u00b9\2\u00bb\2\u00bd\2\3\2\33\b\2HHTTWWhh")
        buf.write("ttww\4\2HHhh\4\2TTtt\4\2DDdd\4\2QQqq\4\2ZZzz\4\2LLll\6")
        buf.write("\2\f\f\16\17))^^\6\2\f\f\16\17$$^^\3\2^^\3\2\63;\3\2\62")
        buf.write(";\3\2\629\5\2\62;CHch\3\2\62\63\4\2GGgg\4\2--//\7\2\2")
        buf.write("\13\r\16\20(*]_\u0081\7\2\2\13\r\16\20#%]_\u0081\4\2\2")
        buf.write("]_\u0081\3\2\2\u0081\4\2\13\13\"\"\4\2\f\f\16\17\u0129")
        buf.write("\2C\\aac|\u00ac\u00ac\u00b7\u00b7\u00bc\u00bc\u00c2\u00d8")
        buf.write("\u00da\u00f8\u00fa\u0243\u0252\u02c3\u02c8\u02d3\u02e2")
        buf.write("\u02e6\u02f0\u02f0\u037c\u037c\u0388\u0388\u038a\u038c")
        buf.write("\u038e\u038e\u0390\u03a3\u03a5\u03d0\u03d2\u03f7\u03f9")
        buf.write("\u0483\u048c\u04d0\u04d2\u04fb\u0502\u0511\u0533\u0558")
        buf.write("\u055b\u055b\u0563\u0589\u05d2\u05ec\u05f2\u05f4\u0623")
        buf.write("\u063c\u0642\u064c\u0670\u0671\u0673\u06d5\u06d7\u06d7")
        buf.write("\u06e7\u06e8\u06f0\u06f1\u06fc\u06fe\u0701\u0701\u0712")
        buf.write("\u0712\u0714\u0731\u074f\u076f\u0782\u07a7\u07b3\u07b3")
        buf.write("\u0906\u093b\u093f\u093f\u0952\u0952\u095a\u0963\u097f")
        buf.write("\u097f\u0987\u098e\u0991\u0992\u0995\u09aa\u09ac\u09b2")
        buf.write("\u09b4\u09b4\u09b8\u09bb\u09bf\u09bf\u09d0\u09d0\u09de")
        buf.write("\u09df\u09e1\u09e3\u09f2\u09f3\u0a07\u0a0c\u0a11\u0a12")
        buf.write("\u0a15\u0a2a\u0a2c\u0a32\u0a34\u0a35\u0a37\u0a38\u0a3a")
        buf.write("\u0a3b\u0a5b\u0a5e\u0a60\u0a60\u0a74\u0a76\u0a87\u0a8f")
        buf.write("\u0a91\u0a93\u0a95\u0aaa\u0aac\u0ab2\u0ab4\u0ab5\u0ab7")
        buf.write("\u0abb\u0abf\u0abf\u0ad2\u0ad2\u0ae2\u0ae3\u0b07\u0b0e")
        buf.write("\u0b11\u0b12\u0b15\u0b2a\u0b2c\u0b32\u0b34\u0b35\u0b37")
        buf.write("\u0b3b\u0b3f\u0b3f\u0b5e\u0b5f\u0b61\u0b63\u0b73\u0b73")
        buf.write("\u0b85\u0b85\u0b87\u0b8c\u0b90\u0b92\u0b94\u0b97\u0b9b")
        buf.write("\u0b9c\u0b9e\u0b9e\u0ba0\u0ba1\u0ba5\u0ba6\u0baa\u0bac")
        buf.write("\u0bb0\u0bbb\u0c07\u0c0e\u0c10\u0c12\u0c14\u0c2a\u0c2c")
        buf.write("\u0c35\u0c37\u0c3b\u0c62\u0c63\u0c87\u0c8e\u0c90\u0c92")
        buf.write("\u0c94\u0caa\u0cac\u0cb5\u0cb7\u0cbb\u0cbf\u0cbf\u0ce0")
        buf.write("\u0ce0\u0ce2\u0ce3\u0d07\u0d0e\u0d10\u0d12\u0d14\u0d2a")
        buf.write("\u0d2c\u0d3b\u0d62\u0d63\u0d87\u0d98\u0d9c\u0db3\u0db5")
        buf.write("\u0dbd\u0dbf\u0dbf\u0dc2\u0dc8\u0e03\u0e32\u0e34\u0e35")
        buf.write("\u0e42\u0e48\u0e83\u0e84\u0e86\u0e86\u0e89\u0e8a\u0e8c")
        buf.write("\u0e8c\u0e8f\u0e8f\u0e96\u0e99\u0e9b\u0ea1\u0ea3\u0ea5")
        buf.write("\u0ea7\u0ea7\u0ea9\u0ea9\u0eac\u0ead\u0eaf\u0eb2\u0eb4")
        buf.write("\u0eb5\u0ebf\u0ebf\u0ec2\u0ec6\u0ec8\u0ec8\u0ede\u0edf")
        buf.write("\u0f02\u0f02\u0f42\u0f49\u0f4b\u0f6c\u0f8a\u0f8d\u1002")
        buf.write("\u1023\u1025\u1029\u102b\u102c\u1052\u1057\u10a2\u10c7")
        buf.write("\u10d2\u10fc\u10fe\u10fe\u1102\u115b\u1161\u11a4\u11aa")
        buf.write("\u11fb\u1202\u124a\u124c\u124f\u1252\u1258\u125a\u125a")
        buf.write("\u125c\u125f\u1262\u128a\u128c\u128f\u1292\u12b2\u12b4")
        buf.write("\u12b7\u12ba\u12c0\u12c2\u12c2\u12c4\u12c7\u12ca\u12d8")
        buf.write("\u12da\u1312\u1314\u1317\u131a\u135c\u1382\u1391\u13a2")
        buf.write("\u13f6\u1403\u166e\u1671\u1678\u1683\u169c\u16a2\u16ec")
        buf.write("\u16f0\u16f2\u1702\u170e\u1710\u1713\u1722\u1733\u1742")
        buf.write("\u1753\u1762\u176e\u1770\u1772\u1782\u17b5\u17d9\u17d9")
        buf.write("\u17de\u17de\u1822\u1879\u1882\u18aa\u1902\u191e\u1952")
        buf.write("\u196f\u1972\u1976\u1982\u19ab\u19c3\u19c9\u1a02\u1a18")
        buf.write("\u1d02\u1dc1\u1e02\u1e9d\u1ea2\u1efb\u1f02\u1f17\u1f1a")
        buf.write("\u1f1f\u1f22\u1f47\u1f4a\u1f4f\u1f52\u1f59\u1f5b\u1f5b")
        buf.write("\u1f5d\u1f5d\u1f5f\u1f5f\u1f61\u1f7f\u1f82\u1fb6\u1fb8")
        buf.write("\u1fbe\u1fc0\u1fc0\u1fc4\u1fc6\u1fc8\u1fce\u1fd2\u1fd5")
        buf.write("\u1fd8\u1fdd\u1fe2\u1fee\u1ff4\u1ff6\u1ff8\u1ffe\u2073")
        buf.write("\u2073\u2081\u2081\u2092\u2096\u2104\u2104\u2109\u2109")
        buf.write("\u210c\u2115\u2117\u2117\u211a\u211f\u2126\u2126\u2128")
        buf.write("\u2128\u212a\u212a\u212c\u2133\u2135\u213b\u213e\u2141")
        buf.write("\u2147\u214b\u2162\u2185\u2c02\u2c30\u2c32\u2c60\u2c82")
        buf.write("\u2ce6\u2d02\u2d27\u2d32\u2d67\u2d71\u2d71\u2d82\u2d98")
        buf.write("\u2da2\u2da8\u2daa\u2db0\u2db2\u2db8\u2dba\u2dc0\u2dc2")
        buf.write("\u2dc8\u2dca\u2dd0\u2dd2\u2dd8\u2dda\u2de0\u3007\u3009")
        buf.write("\u3023\u302b\u3033\u3037\u303a\u303e\u3043\u3098\u309d")
        buf.write("\u30a1\u30a3\u30fc\u30fe\u3101\u3107\u312e\u3133\u3190")
        buf.write("\u31a2\u31b9\u31f2\u3201\u3402\u4db7\u4e02\u9fbd\ua002")
        buf.write("\ua48e\ua802\ua803\ua805\ua807\ua809\ua80c\ua80e\ua824")
        buf.write("\uac02\ud7a5\uf902\ufa2f\ufa32\ufa6c\ufa72\ufadb\ufb02")
        buf.write("\ufb08\ufb15\ufb19\ufb1f\ufb1f\ufb21\ufb2a\ufb2c\ufb38")
        buf.write("\ufb3a\ufb3e\ufb40\ufb40\ufb42\ufb43\ufb45\ufb46\ufb48")
        buf.write("\ufbb3\ufbd5\ufd3f\ufd52\ufd91\ufd94\ufdc9\ufdf2\ufdfd")
        buf.write("\ufe72\ufe76\ufe78\ufefe\uff23\uff3c\uff43\uff5c\uff68")
        buf.write("\uffc0\uffc4\uffc9\uffcc\uffd1\uffd4\uffd9\uffdc\uffde")
        buf.write("\u0096\2\62;\u0302\u0371\u0485\u0488\u0593\u05bb\u05bd")
        buf.write("\u05bf\u05c1\u05c1\u05c3\u05c4\u05c6\u05c7\u05c9\u05c9")
        buf.write("\u0612\u0617\u064d\u0660\u0662\u066b\u0672\u0672\u06d8")
        buf.write("\u06de\u06e1\u06e6\u06e9\u06ea\u06ec\u06ef\u06f2\u06fb")
        buf.write("\u0713\u0713\u0732\u074c\u07a8\u07b2\u0903\u0905\u093e")
        buf.write("\u093e\u0940\u094f\u0953\u0956\u0964\u0965\u0968\u0971")
        buf.write("\u0983\u0985\u09be\u09be\u09c0\u09c6\u09c9\u09ca\u09cd")
        buf.write("\u09cf\u09d9\u09d9\u09e4\u09e5\u09e8\u09f1\u0a03\u0a05")
        buf.write("\u0a3e\u0a3e\u0a40\u0a44\u0a49\u0a4a\u0a4d\u0a4f\u0a68")
        buf.write("\u0a73\u0a83\u0a85\u0abe\u0abe\u0ac0\u0ac7\u0ac9\u0acb")
        buf.write("\u0acd\u0acf\u0ae4\u0ae5\u0ae8\u0af1\u0b03\u0b05\u0b3e")
        buf.write("\u0b3e\u0b40\u0b45\u0b49\u0b4a\u0b4d\u0b4f\u0b58\u0b59")
        buf.write("\u0b68\u0b71\u0b84\u0b84\u0bc0\u0bc4\u0bc8\u0bca\u0bcc")
        buf.write("\u0bcf\u0bd9\u0bd9\u0be8\u0bf1\u0c03\u0c05\u0c40\u0c46")
        buf.write("\u0c48\u0c4a\u0c4c\u0c4f\u0c57\u0c58\u0c68\u0c71\u0c84")
        buf.write("\u0c85\u0cbe\u0cbe\u0cc0\u0cc6\u0cc8\u0cca\u0ccc\u0ccf")
        buf.write("\u0cd7\u0cd8\u0ce8\u0cf1\u0d04\u0d05\u0d40\u0d45\u0d48")
        buf.write("\u0d4a\u0d4c\u0d4f\u0d59\u0d59\u0d68\u0d71\u0d84\u0d85")
        buf.write("\u0dcc\u0dcc\u0dd1\u0dd6\u0dd8\u0dd8\u0dda\u0de1\u0df4")
        buf.write("\u0df5\u0e33\u0e33\u0e36\u0e3c\u0e49\u0e50\u0e52\u0e5b")
        buf.write("\u0eb3\u0eb3\u0eb6\u0ebb\u0ebd\u0ebe\u0eca\u0ecf\u0ed2")
        buf.write("\u0edb\u0f1a\u0f1b\u0f22\u0f2b\u0f37\u0f37\u0f39\u0f39")
        buf.write("\u0f3b\u0f3b\u0f40\u0f41\u0f73\u0f86\u0f88\u0f89\u0f92")
        buf.write("\u0f99\u0f9b\u0fbe\u0fc8\u0fc8\u102e\u1034\u1038\u103b")
        buf.write("\u1042\u104b\u1058\u105b\u1361\u1361\u136b\u1373\u1714")
        buf.write("\u1716\u1734\u1736\u1754\u1755\u1774\u1775\u17b8\u17d5")
        buf.write("\u17df\u17df\u17e2\u17eb\u180d\u180f\u1812\u181b\u18ab")
        buf.write("\u18ab\u1922\u192d\u1932\u193d\u1948\u1951\u19b2\u19c2")
        buf.write("\u19ca\u19cb\u19d2\u19db\u1a19\u1a1d\u1dc2\u1dc5\u2041")
        buf.write("\u2042\u2056\u2056\u20d2\u20de\u20e3\u20e3\u20e7\u20ed")
        buf.write("\u302c\u3031\u309b\u309c\ua804\ua804\ua808\ua808\ua80d")
        buf.write("\ua80d\ua825\ua829\ufb20\ufb20\ufe02\ufe11\ufe22\ufe25")
        buf.write("\ufe35\ufe36\ufe4f\ufe51\uff12\uff1b\uff41\uff41\2\u02d0")
        buf.write("\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13")
        buf.write("\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3")
        buf.write("\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2")
        buf.write("\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2")
        buf.write("%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2")
        buf.write("\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67")
        buf.write("\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2")
        buf.write("A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2")
        buf.write("\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2")
        buf.write("\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2")
        buf.write("\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3")
        buf.write("\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q")
        buf.write("\3\2\2\2\2s\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\2")
        buf.write("{\3\2\2\2\2}\3\2\2\2\2\177\3\2\2\2\2\u0081\3\2\2\2\2\u0083")
        buf.write("\3\2\2\2\2\u0085\3\2\2\2\2\u0087\3\2\2\2\3\u00bf\3\2\2")
        buf.write("\2\5\u00c3\3\2\2\2\7\u00c5\3\2\2\2\t\u00c7\3\2\2\2\13")
        buf.write("\u00c9\3\2\2\2\r\u00cb\3\2\2\2\17\u00d2\3\2\2\2\21\u00d4")
        buf.write("\3\2\2\2\23\u00d7\3\2\2\2\25\u00da\3\2\2\2\27\u00dd\3")
        buf.write("\2\2\2\31\u00e0\3\2\2\2\33\u00e3\3\2\2\2\35\u00e6\3\2")
        buf.write("\2\2\37\u00e9\3\2\2\2!\u00ec\3\2\2\2#\u00ef\3\2\2\2%\u00f3")
        buf.write("\3\2\2\2\'\u00f7\3\2\2\2)\u00fb\3\2\2\2+\u00ff\3\2\2\2")
        buf.write("-\u0103\3\2\2\2/\u0106\3\2\2\2\61\u010c\3\2\2\2\63\u010f")
        buf.write("\3\2\2\2\65\u0115\3\2\2\2\67\u011a\3\2\2\29\u0120\3\2")
        buf.write("\2\2;\u0129\3\2\2\2=\u0130\3\2\2\2?\u0135\3\2\2\2A\u0137")
        buf.write("\3\2\2\2C\u013b\3\2\2\2E\u013d\3\2\2\2G\u0140\3\2\2\2")
        buf.write("I\u0143\3\2\2\2K\u0145\3\2\2\2M\u0147\3\2\2\2O\u0149\3")
        buf.write("\2\2\2Q\u014b\3\2\2\2S\u014d\3\2\2\2U\u0150\3\2\2\2W\u0153")
        buf.write("\3\2\2\2Y\u0156\3\2\2\2[\u0159\3\2\2\2]\u015c\3\2\2\2")
        buf.write("_\u0160\3\2\2\2a\u0163\3\2\2\2c\u0165\3\2\2\2e\u0167\3")
        buf.write("\2\2\2g\u016c\3\2\2\2i\u0172\3\2\2\2k\u0179\3\2\2\2m\u017e")
        buf.write("\3\2\2\2o\u0184\3\2\2\2q\u0192\3\2\2\2s\u0196\3\2\2\2")
        buf.write("u\u01a2\3\2\2\2w\u01ad\3\2\2\2y\u01bf\3\2\2\2{\u01c1\3")
        buf.write("\2\2\2}\u01c8\3\2\2\2\177\u01cf\3\2\2\2\u0081\u01d8\3")
        buf.write("\2\2\2\u0083\u01dc\3\2\2\2\u0085\u01e3\3\2\2\2\u0087\u01e7")
        buf.write("\3\2\2\2\u0089\u01fb\3\2\2\2\u008b\u0217\3\2\2\2\u008d")
        buf.write("\u021b\3\2\2\2\u008f\u021d\3\2\2\2\u0091\u0223\3\2\2\2")
        buf.write("\u0093\u0225\3\2\2\2\u0095\u0227\3\2\2\2\u0097\u0229\3")
        buf.write("\2\2\2\u0099\u022b\3\2\2\2\u009b\u022d\3\2\2\2\u009d\u0236")
        buf.write("\3\2\2\2\u009f\u023a\3\2\2\2\u00a1\u023f\3\2\2\2\u00a3")
        buf.write("\u0243\3\2\2\2\u00a5\u0249\3\2\2\2\u00a7\u0264\3\2\2\2")
        buf.write("\u00a9\u0280\3\2\2\2\u00ab\u0284\3\2\2\2\u00ad\u0287\3")
        buf.write("\2\2\2\u00af\u028a\3\2\2\2\u00b1\u028d\3\2\2\2\u00b3\u028f")
        buf.write("\3\2\2\2\u00b5\u0293\3\2\2\2\u00b7\u0297\3\2\2\2\u00b9")
        buf.write("\u029e\3\2\2\2\u00bb\u02aa\3\2\2\2\u00bd\u02ae\3\2\2\2")
        buf.write("\u00bf\u00c0\7f\2\2\u00c0\u00c1\7g\2\2\u00c1\u00c2\7h")
        buf.write("\2\2\u00c2\4\3\2\2\2\u00c3\u00c4\7*\2\2\u00c4\6\3\2\2")
        buf.write("\2\u00c5\u00c6\7+\2\2\u00c6\b\3\2\2\2\u00c7\u00c8\7<\2")
        buf.write("\2\u00c8\n\3\2\2\2\u00c9\u00ca\7.\2\2\u00ca\f\3\2\2\2")
        buf.write("\u00cb\u00cc\7t\2\2\u00cc\u00cd\7g\2\2\u00cd\u00ce\7v")
        buf.write("\2\2\u00ce\u00cf\7w\2\2\u00cf\u00d0\7t\2\2\u00d0\u00d1")
        buf.write("\7p\2\2\u00d1\16\3\2\2\2\u00d2\u00d3\7?\2\2\u00d3\20\3")
        buf.write("\2\2\2\u00d4\u00d5\7-\2\2\u00d5\u00d6\7?\2\2\u00d6\22")
        buf.write("\3\2\2\2\u00d7\u00d8\7/\2\2\u00d8\u00d9\7?\2\2\u00d9\24")
        buf.write("\3\2\2\2\u00da\u00db\7,\2\2\u00db\u00dc\7?\2\2\u00dc\26")
        buf.write("\3\2\2\2\u00dd\u00de\7B\2\2\u00de\u00df\7?\2\2\u00df\30")
        buf.write("\3\2\2\2\u00e0\u00e1\7\61\2\2\u00e1\u00e2\7?\2\2\u00e2")
        buf.write("\32\3\2\2\2\u00e3\u00e4\7\'\2\2\u00e4\u00e5\7?\2\2\u00e5")
        buf.write("\34\3\2\2\2\u00e6\u00e7\7(\2\2\u00e7\u00e8\7?\2\2\u00e8")
        buf.write("\36\3\2\2\2\u00e9\u00ea\7~\2\2\u00ea\u00eb\7?\2\2\u00eb")
        buf.write(" \3\2\2\2\u00ec\u00ed\7`\2\2\u00ed\u00ee\7?\2\2\u00ee")
        buf.write("\"\3\2\2\2\u00ef\u00f0\7>\2\2\u00f0\u00f1\7>\2\2\u00f1")
        buf.write("\u00f2\7?\2\2\u00f2$\3\2\2\2\u00f3\u00f4\7@\2\2\u00f4")
        buf.write("\u00f5\7@\2\2\u00f5\u00f6\7?\2\2\u00f6&\3\2\2\2\u00f7")
        buf.write("\u00f8\7,\2\2\u00f8\u00f9\7,\2\2\u00f9\u00fa\7?\2\2\u00fa")
        buf.write("(\3\2\2\2\u00fb\u00fc\7\61\2\2\u00fc\u00fd\7\61\2\2\u00fd")
        buf.write("\u00fe\7?\2\2\u00fe*\3\2\2\2\u00ff\u0100\7h\2\2\u0100")
        buf.write("\u0101\7q\2\2\u0101\u0102\7t\2\2\u0102,\3\2\2\2\u0103")
        buf.write("\u0104\7k\2\2\u0104\u0105\7p\2\2\u0105.\3\2\2\2\u0106")
        buf.write("\u0107\7y\2\2\u0107\u0108\7j\2\2\u0108\u0109\7k\2\2\u0109")
        buf.write("\u010a\7n\2\2\u010a\u010b\7g\2\2\u010b\60\3\2\2\2\u010c")
        buf.write("\u010d\7k\2\2\u010d\u010e\7h\2\2\u010e\62\3\2\2\2\u010f")
        buf.write("\u0110\7g\2\2\u0110\u0111\7n\2\2\u0111\u0112\7u\2\2\u0112")
        buf.write("\u0113\7g\2\2\u0113\u0114\7<\2\2\u0114\64\3\2\2\2\u0115")
        buf.write("\u0116\7r\2\2\u0116\u0117\7c\2\2\u0117\u0118\7u\2\2\u0118")
        buf.write("\u0119\7u\2\2\u0119\66\3\2\2\2\u011a\u011b\7d\2\2\u011b")
        buf.write("\u011c\7t\2\2\u011c\u011d\7g\2\2\u011d\u011e\7c\2\2\u011e")
        buf.write("\u011f\7m\2\2\u011f8\3\2\2\2\u0120\u0121\7e\2\2\u0121")
        buf.write("\u0122\7q\2\2\u0122\u0123\7p\2\2\u0123\u0124\7v\2\2\u0124")
        buf.write("\u0125\7k\2\2\u0125\u0126\7p\2\2\u0126\u0127\7w\2\2\u0127")
        buf.write("\u0128\7g\2\2\u0128:\3\2\2\2\u0129\u012a\7k\2\2\u012a")
        buf.write("\u012b\7o\2\2\u012b\u012c\7r\2\2\u012c\u012d\7q\2\2\u012d")
        buf.write("\u012e\7t\2\2\u012e\u012f\7v\2\2\u012f<\3\2\2\2\u0130")
        buf.write("\u0131\7h\2\2\u0131\u0132\7t\2\2\u0132\u0133\7q\2\2\u0133")
        buf.write("\u0134\7o\2\2\u0134>\3\2\2\2\u0135\u0136\7\60\2\2\u0136")
        buf.write("@\3\2\2\2\u0137\u0138\7\60\2\2\u0138\u0139\7\60\2\2\u0139")
        buf.write("\u013a\7\60\2\2\u013aB\3\2\2\2\u013b\u013c\7,\2\2\u013c")
        buf.write("D\3\2\2\2\u013d\u013e\7c\2\2\u013e\u013f\7u\2\2\u013f")
        buf.write("F\3\2\2\2\u0140\u0141\7,\2\2\u0141\u0142\7,\2\2\u0142")
        buf.write("H\3\2\2\2\u0143\u0144\7-\2\2\u0144J\3\2\2\2\u0145\u0146")
        buf.write("\7/\2\2\u0146L\3\2\2\2\u0147\u0148\7\61\2\2\u0148N\3\2")
        buf.write("\2\2\u0149\u014a\7>\2\2\u014aP\3\2\2\2\u014b\u014c\7@")
        buf.write("\2\2\u014cR\3\2\2\2\u014d\u014e\7?\2\2\u014e\u014f\7?")
        buf.write("\2\2\u014fT\3\2\2\2\u0150\u0151\7@\2\2\u0151\u0152\7?")
        buf.write("\2\2\u0152V\3\2\2\2\u0153\u0154\7>\2\2\u0154\u0155\7?")
        buf.write("\2\2\u0155X\3\2\2\2\u0156\u0157\7>\2\2\u0157\u0158\7@")
        buf.write("\2\2\u0158Z\3\2\2\2\u0159\u015a\7#\2\2\u015a\u015b\7?")
        buf.write("\2\2\u015b\\\3\2\2\2\u015c\u015d\7p\2\2\u015d\u015e\7")
        buf.write("q\2\2\u015e\u015f\7v\2\2\u015f^\3\2\2\2\u0160\u0161\7")
        buf.write("k\2\2\u0161\u0162\7u\2\2\u0162`\3\2\2\2\u0163\u0164\7")
        buf.write("]\2\2\u0164b\3\2\2\2\u0165\u0166\7_\2\2\u0166d\3\2\2\2")
        buf.write("\u0167\u0168\7V\2\2\u0168\u0169\7t\2\2\u0169\u016a\7w")
        buf.write("\2\2\u016a\u016b\7g\2\2\u016bf\3\2\2\2\u016c\u016d\7H")
        buf.write("\2\2\u016d\u016e\7c\2\2\u016e\u016f\7n\2\2\u016f\u0170")
        buf.write("\7u\2\2\u0170\u0171\7g\2\2\u0171h\3\2\2\2\u0172\u0173")
        buf.write("\7P\2\2\u0173\u0174\7q\2\2\u0174\u0175\7p\2\2\u0175\u0176")
        buf.write("\7g\2\2\u0176j\3\2\2\2\u0177\u017a\5u;\2\u0178\u017a\5")
        buf.write("w<\2\u0179\u0177\3\2\2\2\u0179\u0178\3\2\2\2\u017al\3")
        buf.write("\2\2\2\u017b\u017f\5o8\2\u017c\u017f\5\u0081A\2\u017d")
        buf.write("\u017f\5\u0083B\2\u017e\u017b\3\2\2\2\u017e\u017c\3\2")
        buf.write("\2\2\u017e\u017d\3\2\2\2\u017fn\3\2\2\2\u0180\u0185\5")
        buf.write("y=\2\u0181\u0185\5{>\2\u0182\u0185\5}?\2\u0183\u0185\5")
        buf.write("\177@\2\u0184\u0180\3\2\2\2\u0184\u0181\3\2\2\2\u0184")
        buf.write("\u0182\3\2\2\2\u0184\u0183\3\2\2\2\u0185p\3\2\2\2\u0186")
        buf.write("\u0187\69\2\2\u0187\u0193\5\u00b5[\2\u0188\u018a\7\17")
        buf.write("\2\2\u0189\u0188\3\2\2\2\u0189\u018a\3\2\2\2\u018a\u018b")
        buf.write("\3\2\2\2\u018b\u018e\7\f\2\2\u018c\u018e\4\16\17\2\u018d")
        buf.write("\u0189\3\2\2\2\u018d\u018c\3\2\2\2\u018e\u0190\3\2\2\2")
        buf.write("\u018f\u0191\5\u00b5[\2\u0190\u018f\3\2\2\2\u0190\u0191")
        buf.write("\3\2\2\2\u0191\u0193\3\2\2\2\u0192\u0186\3\2\2\2\u0192")
        buf.write("\u018d\3\2\2\2\u0193\u0194\3\2\2\2\u0194\u0195\b9\2\2")
        buf.write("\u0195r\3\2\2\2\u0196\u019a\5\u00bb^\2\u0197\u0199\5\u00bd")
        buf.write("_\2\u0198\u0197\3\2\2\2\u0199\u019c\3\2\2\2\u019a\u0198")
        buf.write("\3\2\2\2\u019a\u019b\3\2\2\2\u019bt\3\2\2\2\u019c\u019a")
        buf.write("\3\2\2\2\u019d\u01a3\t\2\2\2\u019e\u019f\t\3\2\2\u019f")
        buf.write("\u01a3\t\4\2\2\u01a0\u01a1\t\4\2\2\u01a1\u01a3\t\3\2\2")
        buf.write("\u01a2\u019d\3\2\2\2\u01a2\u019e\3\2\2\2\u01a2\u01a0\3")
        buf.write("\2\2\2\u01a2\u01a3\3\2\2\2\u01a3\u01a6\3\2\2\2\u01a4\u01a7")
        buf.write("\5\u0089E\2\u01a5\u01a7\5\u008bF\2\u01a6\u01a4\3\2\2\2")
        buf.write("\u01a6\u01a5\3\2\2\2\u01a7v\3\2\2\2\u01a8\u01ae\t\5\2")
        buf.write("\2\u01a9\u01aa\t\5\2\2\u01aa\u01ae\t\4\2\2\u01ab\u01ac")
        buf.write("\t\4\2\2\u01ac\u01ae\t\5\2\2\u01ad\u01a8\3\2\2\2\u01ad")
        buf.write("\u01a9\3\2\2\2\u01ad\u01ab\3\2\2\2\u01ae\u01b1\3\2\2\2")
        buf.write("\u01af\u01b2\5\u00a7T\2\u01b0\u01b2\5\u00a9U\2\u01b1\u01af")
        buf.write("\3\2\2\2\u01b1\u01b0\3\2\2\2\u01b2x\3\2\2\2\u01b3\u01b7")
        buf.write("\5\u0093J\2\u01b4\u01b6\5\u0095K\2\u01b5\u01b4\3\2\2\2")
        buf.write("\u01b6\u01b9\3\2\2\2\u01b7\u01b5\3\2\2\2\u01b7\u01b8\3")
        buf.write("\2\2\2\u01b8\u01c0\3\2\2\2\u01b9\u01b7\3\2\2\2\u01ba\u01bc")
        buf.write("\7\62\2\2\u01bb\u01ba\3\2\2\2\u01bc\u01bd\3\2\2\2\u01bd")
        buf.write("\u01bb\3\2\2\2\u01bd\u01be\3\2\2\2\u01be\u01c0\3\2\2\2")
        buf.write("\u01bf\u01b3\3\2\2\2\u01bf\u01bb\3\2\2\2\u01c0z\3\2\2")
        buf.write("\2\u01c1\u01c2\7\62\2\2\u01c2\u01c4\t\6\2\2\u01c3\u01c5")
        buf.write("\5\u0097L\2\u01c4\u01c3\3\2\2\2\u01c5\u01c6\3\2\2\2\u01c6")
        buf.write("\u01c4\3\2\2\2\u01c6\u01c7\3\2\2\2\u01c7|\3\2\2\2\u01c8")
        buf.write("\u01c9\7\62\2\2\u01c9\u01cb\t\7\2\2\u01ca\u01cc\5\u0099")
        buf.write("M\2\u01cb\u01ca\3\2\2\2\u01cc\u01cd\3\2\2\2\u01cd\u01cb")
        buf.write("\3\2\2\2\u01cd\u01ce\3\2\2\2\u01ce~\3\2\2\2\u01cf\u01d0")
        buf.write("\7\62\2\2\u01d0\u01d2\t\5\2\2\u01d1\u01d3\5\u009bN\2\u01d2")
        buf.write("\u01d1\3\2\2\2\u01d3\u01d4\3\2\2\2\u01d4\u01d2\3\2\2\2")
        buf.write("\u01d4\u01d5\3\2\2\2\u01d5\u0080\3\2\2\2\u01d6\u01d9\5")
        buf.write("\u009dO\2\u01d7\u01d9\5\u009fP\2\u01d8\u01d6\3\2\2\2\u01d8")
        buf.write("\u01d7\3\2\2\2\u01d9\u0082\3\2\2\2\u01da\u01dd\5\u0081")
        buf.write("A\2\u01db\u01dd\5\u00a1Q\2\u01dc\u01da\3\2\2\2\u01dc\u01db")
        buf.write("\3\2\2\2\u01dd\u01de\3\2\2\2\u01de\u01df\t\b\2\2\u01df")
        buf.write("\u0084\3\2\2\2\u01e0\u01e4\5\u00b5[\2\u01e1\u01e4\5\u00b7")
        buf.write("\\\2\u01e2\u01e4\5\u00b9]\2\u01e3\u01e0\3\2\2\2\u01e3")
        buf.write("\u01e1\3\2\2\2\u01e3\u01e2\3\2\2\2\u01e4\u01e5\3\2\2\2")
        buf.write("\u01e5\u01e6\bC\3\2\u01e6\u0086\3\2\2\2\u01e7\u01e8\13")
        buf.write("\2\2\2\u01e8\u0088\3\2\2\2\u01e9\u01ee\7)\2\2\u01ea\u01ed")
        buf.write("\5\u0091I\2\u01eb\u01ed\n\t\2\2\u01ec\u01ea\3\2\2\2\u01ec")
        buf.write("\u01eb\3\2\2\2\u01ed\u01f0\3\2\2\2\u01ee\u01ec\3\2\2\2")
        buf.write("\u01ee\u01ef\3\2\2\2\u01ef\u01f1\3\2\2\2\u01f0\u01ee\3")
        buf.write("\2\2\2\u01f1\u01fc\7)\2\2\u01f2\u01f7\7$\2\2\u01f3\u01f6")
        buf.write("\5\u0091I\2\u01f4\u01f6\n\n\2\2\u01f5\u01f3\3\2\2\2\u01f5")
        buf.write("\u01f4\3\2\2\2\u01f6\u01f9\3\2\2\2\u01f7\u01f5\3\2\2\2")
        buf.write("\u01f7\u01f8\3\2\2\2\u01f8\u01fa\3\2\2\2\u01f9\u01f7\3")
        buf.write("\2\2\2\u01fa\u01fc\7$\2\2\u01fb\u01e9\3\2\2\2\u01fb\u01f2")
        buf.write("\3\2\2\2\u01fc\u008a\3\2\2\2\u01fd\u01fe\7)\2\2\u01fe")
        buf.write("\u01ff\7)\2\2\u01ff\u0200\7)\2\2\u0200\u0204\3\2\2\2\u0201")
        buf.write("\u0203\5\u008dG\2\u0202\u0201\3\2\2\2\u0203\u0206\3\2")
        buf.write("\2\2\u0204\u0205\3\2\2\2\u0204\u0202\3\2\2\2\u0205\u0207")
        buf.write("\3\2\2\2\u0206\u0204\3\2\2\2\u0207\u0208\7)\2\2\u0208")
        buf.write("\u0209\7)\2\2\u0209\u0218\7)\2\2\u020a\u020b\7$\2\2\u020b")
        buf.write("\u020c\7$\2\2\u020c\u020d\7$\2\2\u020d\u0211\3\2\2\2\u020e")
        buf.write("\u0210\5\u008dG\2\u020f\u020e\3\2\2\2\u0210\u0213\3\2")
        buf.write("\2\2\u0211\u0212\3\2\2\2\u0211\u020f\3\2\2\2\u0212\u0214")
        buf.write("\3\2\2\2\u0213\u0211\3\2\2\2\u0214\u0215\7$\2\2\u0215")
        buf.write("\u0216\7$\2\2\u0216\u0218\7$\2\2\u0217\u01fd\3\2\2\2\u0217")
        buf.write("\u020a\3\2\2\2\u0218\u008c\3\2\2\2\u0219\u021c\5\u008f")
        buf.write("H\2\u021a\u021c\5\u0091I\2\u021b\u0219\3\2\2\2\u021b\u021a")
        buf.write("\3\2\2\2\u021c\u008e\3\2\2\2\u021d\u021e\n\13\2\2\u021e")
        buf.write("\u0090\3\2\2\2\u021f\u0220\7^\2\2\u0220\u0224\13\2\2\2")
        buf.write("\u0221\u0222\7^\2\2\u0222\u0224\5q9\2\u0223\u021f\3\2")
        buf.write("\2\2\u0223\u0221\3\2\2\2\u0224\u0092\3\2\2\2\u0225\u0226")
        buf.write("\t\f\2\2\u0226\u0094\3\2\2\2\u0227\u0228\t\r\2\2\u0228")
        buf.write("\u0096\3\2\2\2\u0229\u022a\t\16\2\2\u022a\u0098\3\2\2")
        buf.write("\2\u022b\u022c\t\17\2\2\u022c\u009a\3\2\2\2\u022d\u022e")
        buf.write("\t\20\2\2\u022e\u009c\3\2\2\2\u022f\u0231\5\u00a1Q\2\u0230")
        buf.write("\u022f\3\2\2\2\u0230\u0231\3\2\2\2\u0231\u0232\3\2\2\2")
        buf.write("\u0232\u0237\5\u00a3R\2\u0233\u0234\5\u00a1Q\2\u0234\u0235")
        buf.write("\7\60\2\2\u0235\u0237\3\2\2\2\u0236\u0230\3\2\2\2\u0236")
        buf.write("\u0233\3\2\2\2\u0237\u009e\3\2\2\2\u0238\u023b\5\u00a1")
        buf.write("Q\2\u0239\u023b\5\u009dO\2\u023a\u0238\3\2\2\2\u023a\u0239")
        buf.write("\3\2\2\2\u023b\u023c\3\2\2\2\u023c\u023d\5\u00a5S\2\u023d")
        buf.write("\u00a0\3\2\2\2\u023e\u0240\5\u0095K\2\u023f\u023e\3\2")
        buf.write("\2\2\u0240\u0241\3\2\2\2\u0241\u023f\3\2\2\2\u0241\u0242")
        buf.write("\3\2\2\2\u0242\u00a2\3\2\2\2\u0243\u0245\7\60\2\2\u0244")
        buf.write("\u0246\5\u0095K\2\u0245\u0244\3\2\2\2\u0246\u0247\3\2")
        buf.write("\2\2\u0247\u0245\3\2\2\2\u0247\u0248\3\2\2\2\u0248\u00a4")
        buf.write("\3\2\2\2\u0249\u024b\t\21\2\2\u024a\u024c\t\22\2\2\u024b")
        buf.write("\u024a\3\2\2\2\u024b\u024c\3\2\2\2\u024c\u024e\3\2\2\2")
        buf.write("\u024d\u024f\5\u0095K\2\u024e\u024d\3\2\2\2\u024f\u0250")
        buf.write("\3\2\2\2\u0250\u024e\3\2\2\2\u0250\u0251\3\2\2\2\u0251")
        buf.write("\u00a6\3\2\2\2\u0252\u0257\7)\2\2\u0253\u0256\5\u00ad")
        buf.write("W\2\u0254\u0256\5\u00b3Z\2\u0255\u0253\3\2\2\2\u0255\u0254")
        buf.write("\3\2\2\2\u0256\u0259\3\2\2\2\u0257\u0255\3\2\2\2\u0257")
        buf.write("\u0258\3\2\2\2\u0258\u025a\3\2\2\2\u0259\u0257\3\2\2\2")
        buf.write("\u025a\u0265\7)\2\2\u025b\u0260\7$\2\2\u025c\u025f\5\u00af")
        buf.write("X\2\u025d\u025f\5\u00b3Z\2\u025e\u025c\3\2\2\2\u025e\u025d")
        buf.write("\3\2\2\2\u025f\u0262\3\2\2\2\u0260\u025e\3\2\2\2\u0260")
        buf.write("\u0261\3\2\2\2\u0261\u0263\3\2\2\2\u0262\u0260\3\2\2\2")
        buf.write("\u0263\u0265\7$\2\2\u0264\u0252\3\2\2\2\u0264\u025b\3")
        buf.write("\2\2\2\u0265\u00a8\3\2\2\2\u0266\u0267\7)\2\2\u0267\u0268")
        buf.write("\7)\2\2\u0268\u0269\7)\2\2\u0269\u026d\3\2\2\2\u026a\u026c")
        buf.write("\5\u00abV\2\u026b\u026a\3\2\2\2\u026c\u026f\3\2\2\2\u026d")
        buf.write("\u026e\3\2\2\2\u026d\u026b\3\2\2\2\u026e\u0270\3\2\2\2")
        buf.write("\u026f\u026d\3\2\2\2\u0270\u0271\7)\2\2\u0271\u0272\7")
        buf.write(")\2\2\u0272\u0281\7)\2\2\u0273\u0274\7$\2\2\u0274\u0275")
        buf.write("\7$\2\2\u0275\u0276\7$\2\2\u0276\u027a\3\2\2\2\u0277\u0279")
        buf.write("\5\u00abV\2\u0278\u0277\3\2\2\2\u0279\u027c\3\2\2\2\u027a")
        buf.write("\u027b\3\2\2\2\u027a\u0278\3\2\2\2\u027b\u027d\3\2\2\2")
        buf.write("\u027c\u027a\3\2\2\2\u027d\u027e\7$\2\2\u027e\u027f\7")
        buf.write("$\2\2\u027f\u0281\7$\2\2\u0280\u0266\3\2\2\2\u0280\u0273")
        buf.write("\3\2\2\2\u0281\u00aa\3\2\2\2\u0282\u0285\5\u00b1Y\2\u0283")
        buf.write("\u0285\5\u00b3Z\2\u0284\u0282\3\2\2\2\u0284\u0283\3\2")
        buf.write("\2\2\u0285\u00ac\3\2\2\2\u0286\u0288\t\23\2\2\u0287\u0286")
        buf.write("\3\2\2\2\u0288\u00ae\3\2\2\2\u0289\u028b\t\24\2\2\u028a")
        buf.write("\u0289\3\2\2\2\u028b\u00b0\3\2\2\2\u028c\u028e\t\25\2")
        buf.write("\2\u028d\u028c\3\2\2\2\u028e\u00b2\3\2\2\2\u028f\u0290")
        buf.write("\7^\2\2\u0290\u0291\t\26\2\2\u0291\u00b4\3\2\2\2\u0292")
        buf.write("\u0294\t\27\2\2\u0293\u0292\3\2\2\2\u0294\u0295\3\2\2")
        buf.write("\2\u0295\u0293\3\2\2\2\u0295\u0296\3\2\2\2\u0296\u00b6")
        buf.write("\3\2\2\2\u0297\u029b\7%\2\2\u0298\u029a\n\30\2\2\u0299")
        buf.write("\u0298\3\2\2\2\u029a\u029d\3\2\2\2\u029b\u0299\3\2\2\2")
        buf.write("\u029b\u029c\3\2\2\2\u029c\u00b8\3\2\2\2\u029d\u029b\3")
        buf.write("\2\2\2\u029e\u02a0\7^\2\2\u029f\u02a1\5\u00b5[\2\u02a0")
        buf.write("\u029f\3\2\2\2\u02a0\u02a1\3\2\2\2\u02a1\u02a7\3\2\2\2")
        buf.write("\u02a2\u02a4\7\17\2\2\u02a3\u02a2\3\2\2\2\u02a3\u02a4")
        buf.write("\3\2\2\2\u02a4\u02a5\3\2\2\2\u02a5\u02a8\7\f\2\2\u02a6")
        buf.write("\u02a8\4\16\17\2\u02a7\u02a3\3\2\2\2\u02a7\u02a6\3\2\2")
        buf.write("\2\u02a8\u00ba\3\2\2\2\u02a9\u02ab\t\31\2\2\u02aa\u02a9")
        buf.write("\3\2\2\2\u02ab\u00bc\3\2\2\2\u02ac\u02af\5\u00bb^\2\u02ad")
        buf.write("\u02af\t\32\2\2\u02ae\u02ac\3\2\2\2\u02ae\u02ad\3\2\2")
        buf.write("\2\u02af\u00be\3\2\2\2<\2\u0179\u017e\u0184\u0189\u018d")
        buf.write("\u0190\u0192\u019a\u01a2\u01a6\u01ad\u01b1\u01b7\u01bd")
        buf.write("\u01bf\u01c6\u01cd\u01d4\u01d8\u01dc\u01e3\u01ec\u01ee")
        buf.write("\u01f5\u01f7\u01fb\u0204\u0211\u0217\u021b\u0223\u0230")
        buf.write("\u0236\u023a\u0241\u0247\u024b\u0250\u0255\u0257\u025e")
        buf.write("\u0260\u0264\u026d\u027a\u0280\u0284\u0287\u028a\u028d")
        buf.write("\u0295\u029b\u02a0\u02a3\u02a7\u02aa\u02ae\4\39\2\b\2")
        buf.write("\2")
        return buf.getvalue()


class MiniPython3Lexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    T__14 = 15
    T__15 = 16
    T__16 = 17
    T__17 = 18
    T__18 = 19
    T__19 = 20
    T__20 = 21
    T__21 = 22
    T__22 = 23
    T__23 = 24
    T__24 = 25
    T__25 = 26
    T__26 = 27
    T__27 = 28
    T__28 = 29
    T__29 = 30
    T__30 = 31
    T__31 = 32
    T__32 = 33
    T__33 = 34
    T__34 = 35
    T__35 = 36
    T__36 = 37
    T__37 = 38
    T__38 = 39
    T__39 = 40
    T__40 = 41
    T__41 = 42
    T__42 = 43
    T__43 = 44
    T__44 = 45
    T__45 = 46
    T__46 = 47
    T__47 = 48
    T__48 = 49
    T__49 = 50
    T__50 = 51
    T__51 = 52
    STRING = 53
    NUMBER = 54
    INTEGER = 55
    NEWLINE = 56
    NAME = 57
    STRING_LITERAL = 58
    BYTES_LITERAL = 59
    DECIMAL_INTEGER = 60
    OCT_INTEGER = 61
    HEX_INTEGER = 62
    BIN_INTEGER = 63
    FLOAT_NUMBER = 64
    IMAG_NUMBER = 65
    SKIP_ = 66
    UNKNOWN_CHAR = 67

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'def'", "'('", "')'", "':'", "','", "'return'", "'='", "'+='", 
            "'-='", "'*='", "'@='", "'/='", "'%='", "'&='", "'|='", "'^='", 
            "'<<='", "'>>='", "'**='", "'//='", "'for'", "'in'", "'while'", 
            "'if'", "'else:'", "'pass'", "'break'", "'continue'", "'import'", 
            "'from'", "'.'", "'...'", "'*'", "'as'", "'**'", "'+'", "'-'", 
            "'/'", "'<'", "'>'", "'=='", "'>='", "'<='", "'<>'", "'!='", 
            "'not'", "'is'", "'['", "']'", "'True'", "'False'", "'None'" ]

    symbolicNames = [ "<INVALID>",
            "STRING", "NUMBER", "INTEGER", "NEWLINE", "NAME", "STRING_LITERAL", 
            "BYTES_LITERAL", "DECIMAL_INTEGER", "OCT_INTEGER", "HEX_INTEGER", 
            "BIN_INTEGER", "FLOAT_NUMBER", "IMAG_NUMBER", "SKIP_", "UNKNOWN_CHAR" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "T__10", "T__11", "T__12", "T__13", 
                  "T__14", "T__15", "T__16", "T__17", "T__18", "T__19", 
                  "T__20", "T__21", "T__22", "T__23", "T__24", "T__25", 
                  "T__26", "T__27", "T__28", "T__29", "T__30", "T__31", 
                  "T__32", "T__33", "T__34", "T__35", "T__36", "T__37", 
                  "T__38", "T__39", "T__40", "T__41", "T__42", "T__43", 
                  "T__44", "T__45", "T__46", "T__47", "T__48", "T__49", 
                  "T__50", "T__51", "STRING", "NUMBER", "INTEGER", "NEWLINE", 
                  "NAME", "STRING_LITERAL", "BYTES_LITERAL", "DECIMAL_INTEGER", 
                  "OCT_INTEGER", "HEX_INTEGER", "BIN_INTEGER", "FLOAT_NUMBER", 
                  "IMAG_NUMBER", "SKIP_", "UNKNOWN_CHAR", "SHORT_STRING", 
                  "LONG_STRING", "LONG_STRING_ITEM", "LONG_STRING_CHAR", 
                  "STRING_ESCAPE_SEQ", "NON_ZERO_DIGIT", "DIGIT", "OCT_DIGIT", 
                  "HEX_DIGIT", "BIN_DIGIT", "POINT_FLOAT", "EXPONENT_FLOAT", 
                  "INT_PART", "FRACTION", "EXPONENT", "SHORT_BYTES", "LONG_BYTES", 
                  "LONG_BYTES_ITEM", "SHORT_BYTES_CHAR_NO_SINGLE_QUOTE", 
                  "SHORT_BYTES_CHAR_NO_DOUBLE_QUOTE", "LONG_BYTES_CHAR", 
                  "BYTES_ESCAPE_SEQ", "SPACES", "COMMENT", "LINE_JOINING", 
                  "ID_START", "ID_CONTINUE" ]

    grammarFileName = "MiniPython3.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    @property
    def tokens(self):
        try:
            return self._tokens
        except AttributeError:
            self._tokens = []
            return self._tokens

    @property
    def indents(self):
        try:
            return self._indents
        except AttributeError:
            self._indents = []
            return self._indents

    @property
    def opened(self):
        try:
            return self._opened
        except AttributeError:
            self._opened = 0
            return self._opened

    @opened.setter
    def opened(self, value):
        self._opened = value

    @property
    def lastToken(self):
        try:
            return self._lastToken
        except AttributeError:
            self._lastToken = None
            return self._lastToken

    @lastToken.setter
    def lastToken(self, value):
        self._lastToken = value

    def reset(self):
        super().reset()
        self.tokens = []
        self.indents = []
        self.opened = 0
        self.lastToken = None

    def emitToken(self, t):
        super().emitToken(t)
        self.tokens.append(t)

    def nextToken(self):
        if self._input.LA(1) == Token.EOF and self.indents:
            for i in range(len(self.tokens)-1,-1,-1):
                if self.tokens[i].type == Token.EOF:
                    self.tokens.pop(i)

            self.emitToken(self.commonToken(LanguageParser.NEWLINE, '\n'))
            while self.indents:
                self.emitToken(self.createDedent())
                self.indents.pop()

            self.emitToken(self.commonToken(LanguageParser.EOF, "<EOF>"))
        next = super().nextToken()
        if next.channel == Token.DEFAULT_CHANNEL:
            self.lastToken = next
        return next if not self.tokens else self.tokens.pop(0)

    def createDedent(self):
        dedent = self.commonToken(LanguageParser.DEDENT, "")
        dedent.line = self.lastToken.line
        return dedent

    def commonToken(self, type, text, indent=0):
        stop = self.getCharIndex()-1-indent
        start = (stop - len(text) + 1) if text else stop
        return CommonToken(self._tokenFactorySourcePair, type, super().DEFAULT_TOKEN_CHANNEL, start, stop)

    @staticmethod
    def getIndentationCount(spaces):
        count = 0
        for ch in spaces:
            if ch == '\t':
                count += 8 - (count % 8)
            else:
                count += 1
        return count

    def atStartOfInput(self):
        return Lexer.column.fget(self) == 0 and Lexer.line.fget(self) == 1


    def action(self, localctx:RuleContext, ruleIndex:int, actionIndex:int):
        if self._actions is None:
            actions = dict()
            actions[55] = self.NEWLINE_action 
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))


    def NEWLINE_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 0:

            tempt = Lexer.text.fget(self)
            newLine = re.sub("[^\r\n\f]+", "", tempt)
            spaces = re.sub("[\r\n\f]+", "", tempt)
            la_char = ""
            try:
                la = self._input.LA(1)
                la_char = chr(la)       # Python does not compare char to ints directly
            except ValueError:          # End of file
                pass

            # Strip newlines inside open clauses except if we are near EOF. We keep NEWLINEs near EOF to
            # satisfy the final newline needed by the single_put rule used by the REPL.
            try:
                nextnext_la = self._input.LA(2)
                nextnext_la_char = chr(nextnext_la)
            except ValueError:
                nextnext_eof = True
            else:
                nextnext_eof = False

            if self.opened > 0 or nextnext_eof is False and (la_char == '\r' or la_char == '\n' or la_char == '\f' or la_char == '#'):
                self.skip()
            else:
                indent = self.getIndentationCount(spaces)
                previous = self.indents[-1] if self.indents else 0
                self.emitToken(self.commonToken(self.NEWLINE, newLine, indent=indent))      # NEWLINE is actually the '\n' char
                if indent == previous:
                    self.skip()
                elif indent > previous:
                    self.indents.append(indent)
                    self.emitToken(self.commonToken(LanguageParser.INDENT, spaces))
                else:
                    while self.indents and self.indents[-1] > indent:
                        self.emitToken(self.createDedent())
                        self.indents.pop()
                
     

    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates is None:
            preds = dict()
            preds[55] = self.NEWLINE_sempred
            self._predicates = preds
        pred = self._predicates.get(ruleIndex, None)
        if pred is not None:
            return pred(localctx, predIndex)
        else:
            raise Exception("No registered predicate for:" + str(ruleIndex))

    def NEWLINE_sempred(self, localctx:RuleContext, predIndex:int):
            if predIndex == 0:
                return self.atStartOfInput()
         


