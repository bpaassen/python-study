import sys
from antlr4 import FileStream
from antlr4 import CommonTokenStream
from antlr4 import ParseTreeWalker
from antlr4.tree.Tree import TerminalNode
from antlr4.tree.Tree import ErrorNode
from MiniPython3Lexer import MiniPython3Lexer
from MiniPython3Parser import MiniPython3Parser
from edist import tree_utils

translations = {
    'module' : 'Module',
    'funcdef' : 'FunctionDef',
    'argument_list' : 'arguments',
    'arg' : 'arg',
    'return_stmt' : 'Return',
    'assign_stmt' : 'Assign',
    'augassign_stmt' : 'AugAssign',
    'for_stmt' : 'For',
    'while_stmt' : 'While',
    'if_stmt' : 'If',
    'if_else_stmt' : 'IfElse',
    'then_stmt' : 'Then',
    'else_stmt' : 'Else',
    'import_stmt' : 'Import',
    'pass_stmt' : 'Pass',
    'break_stmt' : 'Break',
    'continue_stmt' : 'Continue',
    'then_stmt' : 'Then',
    'else_stmt' : 'Else',
    'binary' : 'BinOp',
    'compare' : 'Compare',
    'unary' : 'UnaryOp',
    'call' : 'Call',
    'attribute' : 'Attribute',
    'subscript' : 'Subscript',
    'num' : 'Num',
    'name' : 'Name',
    'name_const' : 'NameConstant',
    'string' : 'String',
    'list_expr' : 'List',
    'tuple_expr' : 'Tuple',
    'subscript_index' : 'Index',
    'or' : 'Or',
    'and' : 'And',
    '+' : 'Add',
    '-' : 'Sub',
    '*' : 'Mult',
    '/' : 'Div',
    '%' : 'Mod',
    '**' : 'Pow',
    'not' : 'Not',
    '+=' : 'Add',
    '-=' : 'Sub',
    '*=' : 'Mult',
    '/=' : 'Div',
    '%=' : 'Mod',
    '**=' : 'Pow',
    '=' : 'Eq',
    '!=' : 'NotEq',
    '<' : 'Lt',
    '<=' : 'LtE',
    '>' : 'Gt',
    '>=' : 'GtE',
    'is' : 'Is',
    'is not' : 'IsNot',
    'in' : 'In',
    'not in' : 'NotIn'
}

def convert_tree(tree, nodes, adj, var, p = -1, i = 0):
    if isinstance(tree, ErrorNode):
        return i
    elif isinstance(tree, TerminalNode):
        return i
    node_name = type(tree).__name__
    node_name = node_name[:len(node_name)-len('Context')].lower()
    print(node_name)
    if node_name in ['num', 'name', 'string', 'arg']:
        varname = tree.getRuleContext().getText()
        reflist = var.setdefault(varname, [])
        reflist.append(i)
    if node_name == 'attribute':
        children = tree.getChildren()
        children.__next__()
        children.__next__()
        child = children.__next__()
        if not isinstance(child, TerminalNode):
            raise ValueError('Expected terminal node as third child of an attribute')
        varname = child.getText()
        print('attribute name: %s' % varname)
        reflist = var.setdefault(varname, [])
        reflist.append(i)
    if node_name in ['plusoperator', 'multoperator', 'poweroperator', 'compoperator', 'unaryoperator', 'augoperator']:
        opname = tree.getRuleContext().getText()
        if node_name == 'unaryoperator':
            if opname == '-':
                opname = 'USub'
            elif opname == '+':
                opname = 'UAdd'
            else:
                opname = 'Not'
        else:
            opname = translations[opname]
        nodes.append(opname)
        adj.append([])
        adj[p].append(i)
        j = i + 1
    elif node_name in translations:
        nodes.append(translations[node_name])
        adj.append([])
        if p >= 0:
            adj[p].append(i)
        j = i + 1
        for child in tree.getChildren():
            j = convert_tree(child, nodes, adj, var, i, j)
    else:
        j = i
        for child in tree.getChildren():
            j = convert_tree(child, nodes, adj, var, p, j)
    return j

def main(argv):
    inp = FileStream(argv[1])
    lexer = MiniPython3Lexer(inp)
    stream = CommonTokenStream(lexer)
    parser = MiniPython3Parser(stream)
    tree = parser.module()

    nodes = []
    adj   = []
    var   = {}
    convert_tree(tree, nodes, adj, var)

    print(tree_utils.tree_to_string(nodes, adj, indent = True, with_indices = True))
    print(var)

if __name__ == '__main__':
    main(sys.argv)
