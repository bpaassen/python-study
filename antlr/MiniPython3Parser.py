# Generated from MiniPython3.g4 by ANTLR 4.8
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3G")
        buf.write("\u01a4\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36")
        buf.write("\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t")
        buf.write("&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\3\2\3\2\7\2[\n")
        buf.write("\2\f\2\16\2^\13\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3}\n\3\3\4\3\4\3\4\3\4\3")
        buf.write("\4\3\4\3\4\3\4\3\5\5\5\u0088\n\5\3\5\3\5\7\5\u008c\n\5")
        buf.write("\f\5\16\5\u008f\13\5\3\6\3\6\3\7\3\7\3\7\6\7\u0096\n\7")
        buf.write("\r\7\16\7\u0097\3\7\3\7\3\b\3\b\5\b\u009e\n\b\3\t\3\t")
        buf.write("\3\t\7\t\u00a3\n\t\f\t\16\t\u00a6\13\t\3\t\5\t\u00a9\n")
        buf.write("\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13\3\f\3\f\3\f\3")
        buf.write("\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16")
        buf.write("\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\21")
        buf.write("\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\5\25\u00d8")
        buf.write("\n\25\3\26\3\26\3\26\3\27\3\27\7\27\u00df\n\27\f\27\16")
        buf.write("\27\u00e2\13\27\3\27\3\27\6\27\u00e6\n\27\r\27\16\27\u00e7")
        buf.write("\5\27\u00ea\n\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\5")
        buf.write("\27\u00f3\n\27\3\30\3\30\3\30\5\30\u00f8\n\30\3\31\3\31")
        buf.write("\3\31\5\31\u00fd\n\31\3\32\3\32\3\32\7\32\u0102\n\32\f")
        buf.write("\32\16\32\u0105\13\32\3\32\5\32\u0108\n\32\3\33\3\33\3")
        buf.write("\33\7\33\u010d\n\33\f\33\16\33\u0110\13\33\3\34\3\34\3")
        buf.write("\34\7\34\u0115\n\34\f\34\16\34\u0118\13\34\3\35\3\35\3")
        buf.write("\36\3\36\3\36\3\36\3\36\5\36\u0121\n\36\3\36\3\36\3\36")
        buf.write("\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36")
        buf.write("\3\36\3\36\7\36\u0133\n\36\f\36\16\36\u0136\13\36\3\37")
        buf.write("\3\37\3 \3 \3!\3!\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"")
        buf.write("\3\"\3\"\3\"\3\"\5\"\u014b\n\"\3#\3#\3$\3$\3$\3$\3$\3")
        buf.write("$\3$\3$\3$\3$\3$\5$\u015a\n$\3$\3$\3$\5$\u015f\n$\3$\3")
        buf.write("$\7$\u0163\n$\f$\16$\u0166\13$\3$\3$\3$\3$\3$\3$\3$\3")
        buf.write("$\3$\7$\u0171\n$\f$\16$\u0174\13$\3%\3%\3%\3%\3%\5%\u017b")
        buf.write("\n%\3&\3&\3\'\3\'\3(\3(\3)\3)\3*\3*\3+\3+\5+\u0189\n+")
        buf.write("\3+\3+\7+\u018d\n+\f+\16+\u0190\13+\3+\5+\u0193\n+\3+")
        buf.write("\3+\3,\3,\3,\3,\6,\u019b\n,\r,\16,\u019c\3,\5,\u01a0\n")
        buf.write(",\3,\3,\3,\2\4:F-\2\4\6\b\n\f\16\20\22\24\26\30\32\34")
        buf.write("\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTV\2\b\3\2\n\26")
        buf.write("\3\2!\"\3\2&\'\4\2##((\4\2&\'\60\60\3\2\64\66\2\u01b8")
        buf.write("\2\\\3\2\2\2\4|\3\2\2\2\6~\3\2\2\2\b\u0087\3\2\2\2\n\u0090")
        buf.write("\3\2\2\2\f\u0092\3\2\2\2\16\u009b\3\2\2\2\20\u009f\3\2")
        buf.write("\2\2\22\u00ad\3\2\2\2\24\u00b1\3\2\2\2\26\u00b3\3\2\2")
        buf.write("\2\30\u00ba\3\2\2\2\32\u00bf\3\2\2\2\34\u00c4\3\2\2\2")
        buf.write("\36\u00cb\3\2\2\2 \u00cd\3\2\2\2\"\u00cf\3\2\2\2$\u00d1")
        buf.write("\3\2\2\2&\u00d3\3\2\2\2(\u00d7\3\2\2\2*\u00d9\3\2\2\2")
        buf.write(",\u00dc\3\2\2\2.\u00f4\3\2\2\2\60\u00f9\3\2\2\2\62\u00fe")
        buf.write("\3\2\2\2\64\u0109\3\2\2\2\66\u0111\3\2\2\28\u0119\3\2")
        buf.write("\2\2:\u0120\3\2\2\2<\u0137\3\2\2\2>\u0139\3\2\2\2@\u013b")
        buf.write("\3\2\2\2B\u014a\3\2\2\2D\u014c\3\2\2\2F\u0159\3\2\2\2")
        buf.write("H\u017a\3\2\2\2J\u017c\3\2\2\2L\u017e\3\2\2\2N\u0180\3")
        buf.write("\2\2\2P\u0182\3\2\2\2R\u0184\3\2\2\2T\u0186\3\2\2\2V\u0196")
        buf.write("\3\2\2\2X[\7:\2\2Y[\5\4\3\2ZX\3\2\2\2ZY\3\2\2\2[^\3\2")
        buf.write("\2\2\\Z\3\2\2\2\\]\3\2\2\2]_\3\2\2\2^\\\3\2\2\2_`\7\2")
        buf.write("\2\3`\3\3\2\2\2a}\5\6\4\2bc\5\16\b\2cd\7:\2\2d}\3\2\2")
        buf.write("\2ef\5\20\t\2fg\7:\2\2g}\3\2\2\2hi\5\22\n\2ij\7:\2\2j")
        buf.write("}\3\2\2\2k}\5\26\f\2l}\5\30\r\2m}\5\32\16\2n}\5\34\17")
        buf.write("\2o}\5(\25\2pq\5\"\22\2qr\7:\2\2r}\3\2\2\2st\5$\23\2t")
        buf.write("u\7:\2\2u}\3\2\2\2vw\5&\24\2wx\7:\2\2x}\3\2\2\2yz\58\35")
        buf.write("\2z{\7:\2\2{}\3\2\2\2|a\3\2\2\2|b\3\2\2\2|e\3\2\2\2|h")
        buf.write("\3\2\2\2|k\3\2\2\2|l\3\2\2\2|m\3\2\2\2|n\3\2\2\2|o\3\2")
        buf.write("\2\2|p\3\2\2\2|s\3\2\2\2|v\3\2\2\2|y\3\2\2\2}\5\3\2\2")
        buf.write("\2~\177\7\3\2\2\177\u0080\5P)\2\u0080\u0081\7\4\2\2\u0081")
        buf.write("\u0082\5\b\5\2\u0082\u0083\7\5\2\2\u0083\u0084\7\6\2\2")
        buf.write("\u0084\u0085\5\f\7\2\u0085\7\3\2\2\2\u0086\u0088\5\n\6")
        buf.write("\2\u0087\u0086\3\2\2\2\u0087\u0088\3\2\2\2\u0088\u008d")
        buf.write("\3\2\2\2\u0089\u008a\7\7\2\2\u008a\u008c\5\n\6\2\u008b")
        buf.write("\u0089\3\2\2\2\u008c\u008f\3\2\2\2\u008d\u008b\3\2\2\2")
        buf.write("\u008d\u008e\3\2\2\2\u008e\t\3\2\2\2\u008f\u008d\3\2\2")
        buf.write("\2\u0090\u0091\7;\2\2\u0091\13\3\2\2\2\u0092\u0093\7:")
        buf.write("\2\2\u0093\u0095\7F\2\2\u0094\u0096\5\4\3\2\u0095\u0094")
        buf.write("\3\2\2\2\u0096\u0097\3\2\2\2\u0097\u0095\3\2\2\2\u0097")
        buf.write("\u0098\3\2\2\2\u0098\u0099\3\2\2\2\u0099\u009a\7G\2\2")
        buf.write("\u009a\r\3\2\2\2\u009b\u009d\7\b\2\2\u009c\u009e\5:\36")
        buf.write("\2\u009d\u009c\3\2\2\2\u009d\u009e\3\2\2\2\u009e\17\3")
        buf.write("\2\2\2\u009f\u00a4\5:\36\2\u00a0\u00a1\7\7\2\2\u00a1\u00a3")
        buf.write("\5:\36\2\u00a2\u00a0\3\2\2\2\u00a3\u00a6\3\2\2\2\u00a4")
        buf.write("\u00a2\3\2\2\2\u00a4\u00a5\3\2\2\2\u00a5\u00a8\3\2\2\2")
        buf.write("\u00a6\u00a4\3\2\2\2\u00a7\u00a9\7\7\2\2\u00a8\u00a7\3")
        buf.write("\2\2\2\u00a8\u00a9\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa\u00ab")
        buf.write("\7\t\2\2\u00ab\u00ac\5:\36\2\u00ac\21\3\2\2\2\u00ad\u00ae")
        buf.write("\5:\36\2\u00ae\u00af\5\24\13\2\u00af\u00b0\5:\36\2\u00b0")
        buf.write("\23\3\2\2\2\u00b1\u00b2\t\2\2\2\u00b2\25\3\2\2\2\u00b3")
        buf.write("\u00b4\7\27\2\2\u00b4\u00b5\5\b\5\2\u00b5\u00b6\7\30\2")
        buf.write("\2\u00b6\u00b7\5:\36\2\u00b7\u00b8\7\6\2\2\u00b8\u00b9")
        buf.write("\5\f\7\2\u00b9\27\3\2\2\2\u00ba\u00bb\7\31\2\2\u00bb\u00bc")
        buf.write("\5:\36\2\u00bc\u00bd\7\6\2\2\u00bd\u00be\5\f\7\2\u00be")
        buf.write("\31\3\2\2\2\u00bf\u00c0\7\32\2\2\u00c0\u00c1\5:\36\2\u00c1")
        buf.write("\u00c2\7\6\2\2\u00c2\u00c3\5\36\20\2\u00c3\33\3\2\2\2")
        buf.write("\u00c4\u00c5\7\32\2\2\u00c5\u00c6\5:\36\2\u00c6\u00c7")
        buf.write("\7\6\2\2\u00c7\u00c8\5\36\20\2\u00c8\u00c9\7\33\2\2\u00c9")
        buf.write("\u00ca\5 \21\2\u00ca\35\3\2\2\2\u00cb\u00cc\5\f\7\2\u00cc")
        buf.write("\37\3\2\2\2\u00cd\u00ce\5\f\7\2\u00ce!\3\2\2\2\u00cf\u00d0")
        buf.write("\7\34\2\2\u00d0#\3\2\2\2\u00d1\u00d2\7\35\2\2\u00d2%\3")
        buf.write("\2\2\2\u00d3\u00d4\7\36\2\2\u00d4\'\3\2\2\2\u00d5\u00d8")
        buf.write("\5*\26\2\u00d6\u00d8\5,\27\2\u00d7\u00d5\3\2\2\2\u00d7")
        buf.write("\u00d6\3\2\2\2\u00d8)\3\2\2\2\u00d9\u00da\7\37\2\2\u00da")
        buf.write("\u00db\5\64\33\2\u00db+\3\2\2\2\u00dc\u00e9\7 \2\2\u00dd")
        buf.write("\u00df\t\3\2\2\u00de\u00dd\3\2\2\2\u00df\u00e2\3\2\2\2")
        buf.write("\u00e0\u00de\3\2\2\2\u00e0\u00e1\3\2\2\2\u00e1\u00e3\3")
        buf.write("\2\2\2\u00e2\u00e0\3\2\2\2\u00e3\u00ea\5\66\34\2\u00e4")
        buf.write("\u00e6\t\3\2\2\u00e5\u00e4\3\2\2\2\u00e6\u00e7\3\2\2\2")
        buf.write("\u00e7\u00e5\3\2\2\2\u00e7\u00e8\3\2\2\2\u00e8\u00ea\3")
        buf.write("\2\2\2\u00e9\u00e0\3\2\2\2\u00e9\u00e5\3\2\2\2\u00ea\u00eb")
        buf.write("\3\2\2\2\u00eb\u00f2\7\37\2\2\u00ec\u00f3\7#\2\2\u00ed")
        buf.write("\u00ee\7\4\2\2\u00ee\u00ef\5\62\32\2\u00ef\u00f0\7\5\2")
        buf.write("\2\u00f0\u00f3\3\2\2\2\u00f1\u00f3\5\62\32\2\u00f2\u00ec")
        buf.write("\3\2\2\2\u00f2\u00ed\3\2\2\2\u00f2\u00f1\3\2\2\2\u00f3")
        buf.write("-\3\2\2\2\u00f4\u00f7\7;\2\2\u00f5\u00f6\7$\2\2\u00f6")
        buf.write("\u00f8\7;\2\2\u00f7\u00f5\3\2\2\2\u00f7\u00f8\3\2\2\2")
        buf.write("\u00f8/\3\2\2\2\u00f9\u00fc\5\66\34\2\u00fa\u00fb\7$\2")
        buf.write("\2\u00fb\u00fd\7;\2\2\u00fc\u00fa\3\2\2\2\u00fc\u00fd")
        buf.write("\3\2\2\2\u00fd\61\3\2\2\2\u00fe\u0103\5.\30\2\u00ff\u0100")
        buf.write("\7\7\2\2\u0100\u0102\5.\30\2\u0101\u00ff\3\2\2\2\u0102")
        buf.write("\u0105\3\2\2\2\u0103\u0101\3\2\2\2\u0103\u0104\3\2\2\2")
        buf.write("\u0104\u0107\3\2\2\2\u0105\u0103\3\2\2\2\u0106\u0108\7")
        buf.write("\7\2\2\u0107\u0106\3\2\2\2\u0107\u0108\3\2\2\2\u0108\63")
        buf.write("\3\2\2\2\u0109\u010e\5\60\31\2\u010a\u010b\7\7\2\2\u010b")
        buf.write("\u010d\5\60\31\2\u010c\u010a\3\2\2\2\u010d\u0110\3\2\2")
        buf.write("\2\u010e\u010c\3\2\2\2\u010e\u010f\3\2\2\2\u010f\65\3")
        buf.write("\2\2\2\u0110\u010e\3\2\2\2\u0111\u0116\7;\2\2\u0112\u0113")
        buf.write("\7!\2\2\u0113\u0115\7;\2\2\u0114\u0112\3\2\2\2\u0115\u0118")
        buf.write("\3\2\2\2\u0116\u0114\3\2\2\2\u0116\u0117\3\2\2\2\u0117")
        buf.write("\67\3\2\2\2\u0118\u0116\3\2\2\2\u0119\u011a\5:\36\2\u011a")
        buf.write("9\3\2\2\2\u011b\u011c\b\36\1\2\u011c\u0121\5F$\2\u011d")
        buf.write("\u011e\5D#\2\u011e\u011f\5:\36\4\u011f\u0121\3\2\2\2\u0120")
        buf.write("\u011b\3\2\2\2\u0120\u011d\3\2\2\2\u0121\u0134\3\2\2\2")
        buf.write("\u0122\u0123\f\7\2\2\u0123\u0124\5<\37\2\u0124\u0125\5")
        buf.write(":\36\7\u0125\u0133\3\2\2\2\u0126\u0127\f\6\2\2\u0127\u0128")
        buf.write("\5@!\2\u0128\u0129\5:\36\7\u0129\u0133\3\2\2\2\u012a\u012b")
        buf.write("\f\5\2\2\u012b\u012c\5> \2\u012c\u012d\5:\36\6\u012d\u0133")
        buf.write("\3\2\2\2\u012e\u012f\f\3\2\2\u012f\u0130\5B\"\2\u0130")
        buf.write("\u0131\5:\36\4\u0131\u0133\3\2\2\2\u0132\u0122\3\2\2\2")
        buf.write("\u0132\u0126\3\2\2\2\u0132\u012a\3\2\2\2\u0132\u012e\3")
        buf.write("\2\2\2\u0133\u0136\3\2\2\2\u0134\u0132\3\2\2\2\u0134\u0135")
        buf.write("\3\2\2\2\u0135;\3\2\2\2\u0136\u0134\3\2\2\2\u0137\u0138")
        buf.write("\7%\2\2\u0138=\3\2\2\2\u0139\u013a\t\4\2\2\u013a?\3\2")
        buf.write("\2\2\u013b\u013c\t\5\2\2\u013cA\3\2\2\2\u013d\u014b\7")
        buf.write(")\2\2\u013e\u014b\7*\2\2\u013f\u014b\7+\2\2\u0140\u014b")
        buf.write("\7,\2\2\u0141\u014b\7-\2\2\u0142\u014b\7.\2\2\u0143\u014b")
        buf.write("\7/\2\2\u0144\u014b\7\30\2\2\u0145\u0146\7\60\2\2\u0146")
        buf.write("\u014b\7\30\2\2\u0147\u014b\7\61\2\2\u0148\u0149\7\61")
        buf.write("\2\2\u0149\u014b\7\60\2\2\u014a\u013d\3\2\2\2\u014a\u013e")
        buf.write("\3\2\2\2\u014a\u013f\3\2\2\2\u014a\u0140\3\2\2\2\u014a")
        buf.write("\u0141\3\2\2\2\u014a\u0142\3\2\2\2\u014a\u0143\3\2\2\2")
        buf.write("\u014a\u0144\3\2\2\2\u014a\u0145\3\2\2\2\u014a\u0147\3")
        buf.write("\2\2\2\u014a\u0148\3\2\2\2\u014bC\3\2\2\2\u014c\u014d")
        buf.write("\t\6\2\2\u014dE\3\2\2\2\u014e\u014f\b$\1\2\u014f\u015a")
        buf.write("\5N(\2\u0150\u015a\5P)\2\u0151\u015a\5R*\2\u0152\u015a")
        buf.write("\5T+\2\u0153\u015a\5V,\2\u0154\u015a\5L\'\2\u0155\u0156")
        buf.write("\7\4\2\2\u0156\u0157\5:\36\2\u0157\u0158\7\5\2\2\u0158")
        buf.write("\u015a\3\2\2\2\u0159\u014e\3\2\2\2\u0159\u0150\3\2\2\2")
        buf.write("\u0159\u0151\3\2\2\2\u0159\u0152\3\2\2\2\u0159\u0153\3")
        buf.write("\2\2\2\u0159\u0154\3\2\2\2\u0159\u0155\3\2\2\2\u015a\u0172")
        buf.write("\3\2\2\2\u015b\u015c\f\f\2\2\u015c\u015e\7\4\2\2\u015d")
        buf.write("\u015f\5:\36\2\u015e\u015d\3\2\2\2\u015e\u015f\3\2\2\2")
        buf.write("\u015f\u0164\3\2\2\2\u0160\u0161\7\7\2\2\u0161\u0163\5")
        buf.write(":\36\2\u0162\u0160\3\2\2\2\u0163\u0166\3\2\2\2\u0164\u0162")
        buf.write("\3\2\2\2\u0164\u0165\3\2\2\2\u0165\u0167\3\2\2\2\u0166")
        buf.write("\u0164\3\2\2\2\u0167\u0171\7\5\2\2\u0168\u0169\f\13\2")
        buf.write("\2\u0169\u016a\7!\2\2\u016a\u0171\7;\2\2\u016b\u016c\f")
        buf.write("\n\2\2\u016c\u016d\7\62\2\2\u016d\u016e\5H%\2\u016e\u016f")
        buf.write("\7\63\2\2\u016f\u0171\3\2\2\2\u0170\u015b\3\2\2\2\u0170")
        buf.write("\u0168\3\2\2\2\u0170\u016b\3\2\2\2\u0171\u0174\3\2\2\2")
        buf.write("\u0172\u0170\3\2\2\2\u0172\u0173\3\2\2\2\u0173G\3\2\2")
        buf.write("\2\u0174\u0172\3\2\2\2\u0175\u017b\5J&\2\u0176\u0177\5")
        buf.write(":\36\2\u0177\u0178\7\6\2\2\u0178\u0179\5:\36\2\u0179\u017b")
        buf.write("\3\2\2\2\u017a\u0175\3\2\2\2\u017a\u0176\3\2\2\2\u017b")
        buf.write("I\3\2\2\2\u017c\u017d\5:\36\2\u017dK\3\2\2\2\u017e\u017f")
        buf.write("\t\7\2\2\u017fM\3\2\2\2\u0180\u0181\78\2\2\u0181O\3\2")
        buf.write("\2\2\u0182\u0183\7;\2\2\u0183Q\3\2\2\2\u0184\u0185\7\67")
        buf.write("\2\2\u0185S\3\2\2\2\u0186\u0188\7\62\2\2\u0187\u0189\5")
        buf.write(":\36\2\u0188\u0187\3\2\2\2\u0188\u0189\3\2\2\2\u0189\u018e")
        buf.write("\3\2\2\2\u018a\u018b\7\7\2\2\u018b\u018d\5:\36\2\u018c")
        buf.write("\u018a\3\2\2\2\u018d\u0190\3\2\2\2\u018e\u018c\3\2\2\2")
        buf.write("\u018e\u018f\3\2\2\2\u018f\u0192\3\2\2\2\u0190\u018e\3")
        buf.write("\2\2\2\u0191\u0193\7\7\2\2\u0192\u0191\3\2\2\2\u0192\u0193")
        buf.write("\3\2\2\2\u0193\u0194\3\2\2\2\u0194\u0195\7\63\2\2\u0195")
        buf.write("U\3\2\2\2\u0196\u0197\7\4\2\2\u0197\u019a\5:\36\2\u0198")
        buf.write("\u0199\7\7\2\2\u0199\u019b\5:\36\2\u019a\u0198\3\2\2\2")
        buf.write("\u019b\u019c\3\2\2\2\u019c\u019a\3\2\2\2\u019c\u019d\3")
        buf.write("\2\2\2\u019d\u019f\3\2\2\2\u019e\u01a0\7\7\2\2\u019f\u019e")
        buf.write("\3\2\2\2\u019f\u01a0\3\2\2\2\u01a0\u01a1\3\2\2\2\u01a1")
        buf.write("\u01a2\7\5\2\2\u01a2W\3\2\2\2%Z\\|\u0087\u008d\u0097\u009d")
        buf.write("\u00a4\u00a8\u00d7\u00e0\u00e7\u00e9\u00f2\u00f7\u00fc")
        buf.write("\u0103\u0107\u010e\u0116\u0120\u0132\u0134\u014a\u0159")
        buf.write("\u015e\u0164\u0170\u0172\u017a\u0188\u018e\u0192\u019c")
        buf.write("\u019f")
        return buf.getvalue()


class MiniPython3Parser ( Parser ):

    grammarFileName = "MiniPython3.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'def'", "'('", "')'", "':'", "','", "'return'", 
                     "'='", "'+='", "'-='", "'*='", "'@='", "'/='", "'%='", 
                     "'&='", "'|='", "'^='", "'<<='", "'>>='", "'**='", 
                     "'//='", "'for'", "'in'", "'while'", "'if'", "'else:'", 
                     "'pass'", "'break'", "'continue'", "'import'", "'from'", 
                     "'.'", "'...'", "'*'", "'as'", "'**'", "'+'", "'-'", 
                     "'/'", "'<'", "'>'", "'=='", "'>='", "'<='", "'<>'", 
                     "'!='", "'not'", "'is'", "'['", "']'", "'True'", "'False'", 
                     "'None'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "STRING", "NUMBER", "INTEGER", "NEWLINE", 
                      "NAME", "STRING_LITERAL", "BYTES_LITERAL", "DECIMAL_INTEGER", 
                      "OCT_INTEGER", "HEX_INTEGER", "BIN_INTEGER", "FLOAT_NUMBER", 
                      "IMAG_NUMBER", "SKIP_", "UNKNOWN_CHAR", "INDENT", 
                      "DEDENT" ]

    RULE_module = 0
    RULE_stmt = 1
    RULE_funcdef = 2
    RULE_argument_list = 3
    RULE_arg = 4
    RULE_suite = 5
    RULE_return_stmt = 6
    RULE_assign_stmt = 7
    RULE_augassign_stmt = 8
    RULE_augoperator = 9
    RULE_for_stmt = 10
    RULE_while_stmt = 11
    RULE_if_stmt = 12
    RULE_if_else_stmt = 13
    RULE_then_stmt = 14
    RULE_else_stmt = 15
    RULE_pass_stmt = 16
    RULE_break_stmt = 17
    RULE_continue_stmt = 18
    RULE_import_stmt = 19
    RULE_import_name = 20
    RULE_import_from = 21
    RULE_import_as_name = 22
    RULE_dotted_as_name = 23
    RULE_import_as_names = 24
    RULE_dotted_as_names = 25
    RULE_dotted_name = 26
    RULE_expr_stmt = 27
    RULE_expr = 28
    RULE_poweroperator = 29
    RULE_plusoperator = 30
    RULE_multoperator = 31
    RULE_compoperator = 32
    RULE_unaryoperator = 33
    RULE_atom = 34
    RULE_slice_expr = 35
    RULE_subscript_index = 36
    RULE_name_const = 37
    RULE_num = 38
    RULE_name = 39
    RULE_string = 40
    RULE_list_expr = 41
    RULE_tuple_expr = 42

    ruleNames =  [ "module", "stmt", "funcdef", "argument_list", "arg", 
                   "suite", "return_stmt", "assign_stmt", "augassign_stmt", 
                   "augoperator", "for_stmt", "while_stmt", "if_stmt", "if_else_stmt", 
                   "then_stmt", "else_stmt", "pass_stmt", "break_stmt", 
                   "continue_stmt", "import_stmt", "import_name", "import_from", 
                   "import_as_name", "dotted_as_name", "import_as_names", 
                   "dotted_as_names", "dotted_name", "expr_stmt", "expr", 
                   "poweroperator", "plusoperator", "multoperator", "compoperator", 
                   "unaryoperator", "atom", "slice_expr", "subscript_index", 
                   "name_const", "num", "name", "string", "list_expr", "tuple_expr" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    T__20=21
    T__21=22
    T__22=23
    T__23=24
    T__24=25
    T__25=26
    T__26=27
    T__27=28
    T__28=29
    T__29=30
    T__30=31
    T__31=32
    T__32=33
    T__33=34
    T__34=35
    T__35=36
    T__36=37
    T__37=38
    T__38=39
    T__39=40
    T__40=41
    T__41=42
    T__42=43
    T__43=44
    T__44=45
    T__45=46
    T__46=47
    T__47=48
    T__48=49
    T__49=50
    T__50=51
    T__51=52
    STRING=53
    NUMBER=54
    INTEGER=55
    NEWLINE=56
    NAME=57
    STRING_LITERAL=58
    BYTES_LITERAL=59
    DECIMAL_INTEGER=60
    OCT_INTEGER=61
    HEX_INTEGER=62
    BIN_INTEGER=63
    FLOAT_NUMBER=64
    IMAG_NUMBER=65
    SKIP_=66
    UNKNOWN_CHAR=67
    INDENT=68
    DEDENT=69

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ModuleContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(MiniPython3Parser.EOF, 0)

        def NEWLINE(self, i:int=None):
            if i is None:
                return self.getTokens(MiniPython3Parser.NEWLINE)
            else:
                return self.getToken(MiniPython3Parser.NEWLINE, i)

        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniPython3Parser.StmtContext)
            else:
                return self.getTypedRuleContext(MiniPython3Parser.StmtContext,i)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_module

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterModule" ):
                listener.enterModule(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitModule" ):
                listener.exitModule(self)




    def module(self):

        localctx = MiniPython3Parser.ModuleContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_module)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 90
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MiniPython3Parser.T__0) | (1 << MiniPython3Parser.T__1) | (1 << MiniPython3Parser.T__5) | (1 << MiniPython3Parser.T__20) | (1 << MiniPython3Parser.T__22) | (1 << MiniPython3Parser.T__23) | (1 << MiniPython3Parser.T__25) | (1 << MiniPython3Parser.T__26) | (1 << MiniPython3Parser.T__27) | (1 << MiniPython3Parser.T__28) | (1 << MiniPython3Parser.T__29) | (1 << MiniPython3Parser.T__35) | (1 << MiniPython3Parser.T__36) | (1 << MiniPython3Parser.T__45) | (1 << MiniPython3Parser.T__47) | (1 << MiniPython3Parser.T__49) | (1 << MiniPython3Parser.T__50) | (1 << MiniPython3Parser.T__51) | (1 << MiniPython3Parser.STRING) | (1 << MiniPython3Parser.NUMBER) | (1 << MiniPython3Parser.NEWLINE) | (1 << MiniPython3Parser.NAME))) != 0):
                self.state = 88
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [MiniPython3Parser.NEWLINE]:
                    self.state = 86
                    self.match(MiniPython3Parser.NEWLINE)
                    pass
                elif token in [MiniPython3Parser.T__0, MiniPython3Parser.T__1, MiniPython3Parser.T__5, MiniPython3Parser.T__20, MiniPython3Parser.T__22, MiniPython3Parser.T__23, MiniPython3Parser.T__25, MiniPython3Parser.T__26, MiniPython3Parser.T__27, MiniPython3Parser.T__28, MiniPython3Parser.T__29, MiniPython3Parser.T__35, MiniPython3Parser.T__36, MiniPython3Parser.T__45, MiniPython3Parser.T__47, MiniPython3Parser.T__49, MiniPython3Parser.T__50, MiniPython3Parser.T__51, MiniPython3Parser.STRING, MiniPython3Parser.NUMBER, MiniPython3Parser.NAME]:
                    self.state = 87
                    self.stmt()
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 92
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 93
            self.match(MiniPython3Parser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def funcdef(self):
            return self.getTypedRuleContext(MiniPython3Parser.FuncdefContext,0)


        def return_stmt(self):
            return self.getTypedRuleContext(MiniPython3Parser.Return_stmtContext,0)


        def NEWLINE(self):
            return self.getToken(MiniPython3Parser.NEWLINE, 0)

        def assign_stmt(self):
            return self.getTypedRuleContext(MiniPython3Parser.Assign_stmtContext,0)


        def augassign_stmt(self):
            return self.getTypedRuleContext(MiniPython3Parser.Augassign_stmtContext,0)


        def for_stmt(self):
            return self.getTypedRuleContext(MiniPython3Parser.For_stmtContext,0)


        def while_stmt(self):
            return self.getTypedRuleContext(MiniPython3Parser.While_stmtContext,0)


        def if_stmt(self):
            return self.getTypedRuleContext(MiniPython3Parser.If_stmtContext,0)


        def if_else_stmt(self):
            return self.getTypedRuleContext(MiniPython3Parser.If_else_stmtContext,0)


        def import_stmt(self):
            return self.getTypedRuleContext(MiniPython3Parser.Import_stmtContext,0)


        def pass_stmt(self):
            return self.getTypedRuleContext(MiniPython3Parser.Pass_stmtContext,0)


        def break_stmt(self):
            return self.getTypedRuleContext(MiniPython3Parser.Break_stmtContext,0)


        def continue_stmt(self):
            return self.getTypedRuleContext(MiniPython3Parser.Continue_stmtContext,0)


        def expr_stmt(self):
            return self.getTypedRuleContext(MiniPython3Parser.Expr_stmtContext,0)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStmt" ):
                listener.enterStmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStmt" ):
                listener.exitStmt(self)




    def stmt(self):

        localctx = MiniPython3Parser.StmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 122
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.state = 95
                self.funcdef()
                pass

            elif la_ == 2:
                self.state = 96
                self.return_stmt()
                self.state = 97
                self.match(MiniPython3Parser.NEWLINE)
                pass

            elif la_ == 3:
                self.state = 99
                self.assign_stmt()
                self.state = 100
                self.match(MiniPython3Parser.NEWLINE)
                pass

            elif la_ == 4:
                self.state = 102
                self.augassign_stmt()
                self.state = 103
                self.match(MiniPython3Parser.NEWLINE)
                pass

            elif la_ == 5:
                self.state = 105
                self.for_stmt()
                pass

            elif la_ == 6:
                self.state = 106
                self.while_stmt()
                pass

            elif la_ == 7:
                self.state = 107
                self.if_stmt()
                pass

            elif la_ == 8:
                self.state = 108
                self.if_else_stmt()
                pass

            elif la_ == 9:
                self.state = 109
                self.import_stmt()
                pass

            elif la_ == 10:
                self.state = 110
                self.pass_stmt()
                self.state = 111
                self.match(MiniPython3Parser.NEWLINE)
                pass

            elif la_ == 11:
                self.state = 113
                self.break_stmt()
                self.state = 114
                self.match(MiniPython3Parser.NEWLINE)
                pass

            elif la_ == 12:
                self.state = 116
                self.continue_stmt()
                self.state = 117
                self.match(MiniPython3Parser.NEWLINE)
                pass

            elif la_ == 13:
                self.state = 119
                self.expr_stmt()
                self.state = 120
                self.match(MiniPython3Parser.NEWLINE)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FuncdefContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def name(self):
            return self.getTypedRuleContext(MiniPython3Parser.NameContext,0)


        def argument_list(self):
            return self.getTypedRuleContext(MiniPython3Parser.Argument_listContext,0)


        def suite(self):
            return self.getTypedRuleContext(MiniPython3Parser.SuiteContext,0)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_funcdef

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFuncdef" ):
                listener.enterFuncdef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFuncdef" ):
                listener.exitFuncdef(self)




    def funcdef(self):

        localctx = MiniPython3Parser.FuncdefContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_funcdef)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 124
            self.match(MiniPython3Parser.T__0)
            self.state = 125
            self.name()
            self.state = 126
            self.match(MiniPython3Parser.T__1)
            self.state = 127
            self.argument_list()
            self.state = 128
            self.match(MiniPython3Parser.T__2)
            self.state = 129
            self.match(MiniPython3Parser.T__3)
            self.state = 130
            self.suite()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Argument_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def arg(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniPython3Parser.ArgContext)
            else:
                return self.getTypedRuleContext(MiniPython3Parser.ArgContext,i)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_argument_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArgument_list" ):
                listener.enterArgument_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArgument_list" ):
                listener.exitArgument_list(self)




    def argument_list(self):

        localctx = MiniPython3Parser.Argument_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_argument_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 133
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MiniPython3Parser.NAME:
                self.state = 132
                self.arg()


            self.state = 139
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MiniPython3Parser.T__4:
                self.state = 135
                self.match(MiniPython3Parser.T__4)
                self.state = 136
                self.arg()
                self.state = 141
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ArgContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(MiniPython3Parser.NAME, 0)

        def getRuleIndex(self):
            return MiniPython3Parser.RULE_arg

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArg" ):
                listener.enterArg(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArg" ):
                listener.exitArg(self)




    def arg(self):

        localctx = MiniPython3Parser.ArgContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_arg)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 142
            self.match(MiniPython3Parser.NAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SuiteContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NEWLINE(self):
            return self.getToken(MiniPython3Parser.NEWLINE, 0)

        def INDENT(self):
            return self.getToken(MiniPython3Parser.INDENT, 0)

        def DEDENT(self):
            return self.getToken(MiniPython3Parser.DEDENT, 0)

        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniPython3Parser.StmtContext)
            else:
                return self.getTypedRuleContext(MiniPython3Parser.StmtContext,i)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_suite

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSuite" ):
                listener.enterSuite(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSuite" ):
                listener.exitSuite(self)




    def suite(self):

        localctx = MiniPython3Parser.SuiteContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_suite)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 144
            self.match(MiniPython3Parser.NEWLINE)
            self.state = 145
            self.match(MiniPython3Parser.INDENT)
            self.state = 147 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 146
                self.stmt()
                self.state = 149 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MiniPython3Parser.T__0) | (1 << MiniPython3Parser.T__1) | (1 << MiniPython3Parser.T__5) | (1 << MiniPython3Parser.T__20) | (1 << MiniPython3Parser.T__22) | (1 << MiniPython3Parser.T__23) | (1 << MiniPython3Parser.T__25) | (1 << MiniPython3Parser.T__26) | (1 << MiniPython3Parser.T__27) | (1 << MiniPython3Parser.T__28) | (1 << MiniPython3Parser.T__29) | (1 << MiniPython3Parser.T__35) | (1 << MiniPython3Parser.T__36) | (1 << MiniPython3Parser.T__45) | (1 << MiniPython3Parser.T__47) | (1 << MiniPython3Parser.T__49) | (1 << MiniPython3Parser.T__50) | (1 << MiniPython3Parser.T__51) | (1 << MiniPython3Parser.STRING) | (1 << MiniPython3Parser.NUMBER) | (1 << MiniPython3Parser.NAME))) != 0)):
                    break

            self.state = 151
            self.match(MiniPython3Parser.DEDENT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Return_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(MiniPython3Parser.ExprContext,0)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_return_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterReturn_stmt" ):
                listener.enterReturn_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitReturn_stmt" ):
                listener.exitReturn_stmt(self)




    def return_stmt(self):

        localctx = MiniPython3Parser.Return_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_return_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 153
            self.match(MiniPython3Parser.T__5)
            self.state = 155
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MiniPython3Parser.T__1) | (1 << MiniPython3Parser.T__35) | (1 << MiniPython3Parser.T__36) | (1 << MiniPython3Parser.T__45) | (1 << MiniPython3Parser.T__47) | (1 << MiniPython3Parser.T__49) | (1 << MiniPython3Parser.T__50) | (1 << MiniPython3Parser.T__51) | (1 << MiniPython3Parser.STRING) | (1 << MiniPython3Parser.NUMBER) | (1 << MiniPython3Parser.NAME))) != 0):
                self.state = 154
                self.expr(0)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Assign_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniPython3Parser.ExprContext)
            else:
                return self.getTypedRuleContext(MiniPython3Parser.ExprContext,i)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_assign_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssign_stmt" ):
                listener.enterAssign_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssign_stmt" ):
                listener.exitAssign_stmt(self)




    def assign_stmt(self):

        localctx = MiniPython3Parser.Assign_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_assign_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 157
            self.expr(0)
            self.state = 162
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,7,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 158
                    self.match(MiniPython3Parser.T__4)
                    self.state = 159
                    self.expr(0) 
                self.state = 164
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,7,self._ctx)

            self.state = 166
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MiniPython3Parser.T__4:
                self.state = 165
                self.match(MiniPython3Parser.T__4)


            self.state = 168
            self.match(MiniPython3Parser.T__6)
            self.state = 169
            self.expr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Augassign_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniPython3Parser.ExprContext)
            else:
                return self.getTypedRuleContext(MiniPython3Parser.ExprContext,i)


        def augoperator(self):
            return self.getTypedRuleContext(MiniPython3Parser.AugoperatorContext,0)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_augassign_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAugassign_stmt" ):
                listener.enterAugassign_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAugassign_stmt" ):
                listener.exitAugassign_stmt(self)




    def augassign_stmt(self):

        localctx = MiniPython3Parser.Augassign_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_augassign_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 171
            self.expr(0)
            self.state = 172
            self.augoperator()
            self.state = 173
            self.expr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AugoperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_augoperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAugoperator" ):
                listener.enterAugoperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAugoperator" ):
                listener.exitAugoperator(self)




    def augoperator(self):

        localctx = MiniPython3Parser.AugoperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_augoperator)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 175
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MiniPython3Parser.T__7) | (1 << MiniPython3Parser.T__8) | (1 << MiniPython3Parser.T__9) | (1 << MiniPython3Parser.T__10) | (1 << MiniPython3Parser.T__11) | (1 << MiniPython3Parser.T__12) | (1 << MiniPython3Parser.T__13) | (1 << MiniPython3Parser.T__14) | (1 << MiniPython3Parser.T__15) | (1 << MiniPython3Parser.T__16) | (1 << MiniPython3Parser.T__17) | (1 << MiniPython3Parser.T__18) | (1 << MiniPython3Parser.T__19))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class For_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def argument_list(self):
            return self.getTypedRuleContext(MiniPython3Parser.Argument_listContext,0)


        def expr(self):
            return self.getTypedRuleContext(MiniPython3Parser.ExprContext,0)


        def suite(self):
            return self.getTypedRuleContext(MiniPython3Parser.SuiteContext,0)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_for_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFor_stmt" ):
                listener.enterFor_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFor_stmt" ):
                listener.exitFor_stmt(self)




    def for_stmt(self):

        localctx = MiniPython3Parser.For_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_for_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 177
            self.match(MiniPython3Parser.T__20)
            self.state = 178
            self.argument_list()
            self.state = 179
            self.match(MiniPython3Parser.T__21)
            self.state = 180
            self.expr(0)
            self.state = 181
            self.match(MiniPython3Parser.T__3)
            self.state = 182
            self.suite()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class While_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(MiniPython3Parser.ExprContext,0)


        def suite(self):
            return self.getTypedRuleContext(MiniPython3Parser.SuiteContext,0)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_while_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWhile_stmt" ):
                listener.enterWhile_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWhile_stmt" ):
                listener.exitWhile_stmt(self)




    def while_stmt(self):

        localctx = MiniPython3Parser.While_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_while_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 184
            self.match(MiniPython3Parser.T__22)
            self.state = 185
            self.expr(0)
            self.state = 186
            self.match(MiniPython3Parser.T__3)
            self.state = 187
            self.suite()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class If_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(MiniPython3Parser.ExprContext,0)


        def then_stmt(self):
            return self.getTypedRuleContext(MiniPython3Parser.Then_stmtContext,0)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_if_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIf_stmt" ):
                listener.enterIf_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIf_stmt" ):
                listener.exitIf_stmt(self)




    def if_stmt(self):

        localctx = MiniPython3Parser.If_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_if_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 189
            self.match(MiniPython3Parser.T__23)
            self.state = 190
            self.expr(0)
            self.state = 191
            self.match(MiniPython3Parser.T__3)
            self.state = 192
            self.then_stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class If_else_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(MiniPython3Parser.ExprContext,0)


        def then_stmt(self):
            return self.getTypedRuleContext(MiniPython3Parser.Then_stmtContext,0)


        def else_stmt(self):
            return self.getTypedRuleContext(MiniPython3Parser.Else_stmtContext,0)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_if_else_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIf_else_stmt" ):
                listener.enterIf_else_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIf_else_stmt" ):
                listener.exitIf_else_stmt(self)




    def if_else_stmt(self):

        localctx = MiniPython3Parser.If_else_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_if_else_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 194
            self.match(MiniPython3Parser.T__23)
            self.state = 195
            self.expr(0)
            self.state = 196
            self.match(MiniPython3Parser.T__3)
            self.state = 197
            self.then_stmt()
            self.state = 198
            self.match(MiniPython3Parser.T__24)
            self.state = 199
            self.else_stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Then_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def suite(self):
            return self.getTypedRuleContext(MiniPython3Parser.SuiteContext,0)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_then_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterThen_stmt" ):
                listener.enterThen_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitThen_stmt" ):
                listener.exitThen_stmt(self)




    def then_stmt(self):

        localctx = MiniPython3Parser.Then_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_then_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 201
            self.suite()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Else_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def suite(self):
            return self.getTypedRuleContext(MiniPython3Parser.SuiteContext,0)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_else_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterElse_stmt" ):
                listener.enterElse_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitElse_stmt" ):
                listener.exitElse_stmt(self)




    def else_stmt(self):

        localctx = MiniPython3Parser.Else_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_else_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 203
            self.suite()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Pass_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_pass_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPass_stmt" ):
                listener.enterPass_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPass_stmt" ):
                listener.exitPass_stmt(self)




    def pass_stmt(self):

        localctx = MiniPython3Parser.Pass_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_pass_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 205
            self.match(MiniPython3Parser.T__25)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Break_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_break_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBreak_stmt" ):
                listener.enterBreak_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBreak_stmt" ):
                listener.exitBreak_stmt(self)




    def break_stmt(self):

        localctx = MiniPython3Parser.Break_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_break_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 207
            self.match(MiniPython3Parser.T__26)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Continue_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_continue_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterContinue_stmt" ):
                listener.enterContinue_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitContinue_stmt" ):
                listener.exitContinue_stmt(self)




    def continue_stmt(self):

        localctx = MiniPython3Parser.Continue_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_continue_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 209
            self.match(MiniPython3Parser.T__27)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Import_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def import_name(self):
            return self.getTypedRuleContext(MiniPython3Parser.Import_nameContext,0)


        def import_from(self):
            return self.getTypedRuleContext(MiniPython3Parser.Import_fromContext,0)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_import_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImport_stmt" ):
                listener.enterImport_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImport_stmt" ):
                listener.exitImport_stmt(self)




    def import_stmt(self):

        localctx = MiniPython3Parser.Import_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_import_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 213
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MiniPython3Parser.T__28]:
                self.state = 211
                self.import_name()
                pass
            elif token in [MiniPython3Parser.T__29]:
                self.state = 212
                self.import_from()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Import_nameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def dotted_as_names(self):
            return self.getTypedRuleContext(MiniPython3Parser.Dotted_as_namesContext,0)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_import_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImport_name" ):
                listener.enterImport_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImport_name" ):
                listener.exitImport_name(self)




    def import_name(self):

        localctx = MiniPython3Parser.Import_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_import_name)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 215
            self.match(MiniPython3Parser.T__28)
            self.state = 216
            self.dotted_as_names()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Import_fromContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def dotted_name(self):
            return self.getTypedRuleContext(MiniPython3Parser.Dotted_nameContext,0)


        def import_as_names(self):
            return self.getTypedRuleContext(MiniPython3Parser.Import_as_namesContext,0)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_import_from

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImport_from" ):
                listener.enterImport_from(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImport_from" ):
                listener.exitImport_from(self)




    def import_from(self):

        localctx = MiniPython3Parser.Import_fromContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_import_from)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 218
            self.match(MiniPython3Parser.T__29)
            self.state = 231
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,12,self._ctx)
            if la_ == 1:
                self.state = 222
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==MiniPython3Parser.T__30 or _la==MiniPython3Parser.T__31:
                    self.state = 219
                    _la = self._input.LA(1)
                    if not(_la==MiniPython3Parser.T__30 or _la==MiniPython3Parser.T__31):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 224
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 225
                self.dotted_name()
                pass

            elif la_ == 2:
                self.state = 227 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 226
                    _la = self._input.LA(1)
                    if not(_la==MiniPython3Parser.T__30 or _la==MiniPython3Parser.T__31):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 229 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==MiniPython3Parser.T__30 or _la==MiniPython3Parser.T__31):
                        break

                pass


            self.state = 233
            self.match(MiniPython3Parser.T__28)
            self.state = 240
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MiniPython3Parser.T__32]:
                self.state = 234
                self.match(MiniPython3Parser.T__32)
                pass
            elif token in [MiniPython3Parser.T__1]:
                self.state = 235
                self.match(MiniPython3Parser.T__1)
                self.state = 236
                self.import_as_names()
                self.state = 237
                self.match(MiniPython3Parser.T__2)
                pass
            elif token in [MiniPython3Parser.NAME]:
                self.state = 239
                self.import_as_names()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Import_as_nameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self, i:int=None):
            if i is None:
                return self.getTokens(MiniPython3Parser.NAME)
            else:
                return self.getToken(MiniPython3Parser.NAME, i)

        def getRuleIndex(self):
            return MiniPython3Parser.RULE_import_as_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImport_as_name" ):
                listener.enterImport_as_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImport_as_name" ):
                listener.exitImport_as_name(self)




    def import_as_name(self):

        localctx = MiniPython3Parser.Import_as_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_import_as_name)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 242
            self.match(MiniPython3Parser.NAME)
            self.state = 245
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MiniPython3Parser.T__33:
                self.state = 243
                self.match(MiniPython3Parser.T__33)
                self.state = 244
                self.match(MiniPython3Parser.NAME)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Dotted_as_nameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def dotted_name(self):
            return self.getTypedRuleContext(MiniPython3Parser.Dotted_nameContext,0)


        def NAME(self):
            return self.getToken(MiniPython3Parser.NAME, 0)

        def getRuleIndex(self):
            return MiniPython3Parser.RULE_dotted_as_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDotted_as_name" ):
                listener.enterDotted_as_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDotted_as_name" ):
                listener.exitDotted_as_name(self)




    def dotted_as_name(self):

        localctx = MiniPython3Parser.Dotted_as_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_dotted_as_name)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 247
            self.dotted_name()
            self.state = 250
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MiniPython3Parser.T__33:
                self.state = 248
                self.match(MiniPython3Parser.T__33)
                self.state = 249
                self.match(MiniPython3Parser.NAME)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Import_as_namesContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def import_as_name(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniPython3Parser.Import_as_nameContext)
            else:
                return self.getTypedRuleContext(MiniPython3Parser.Import_as_nameContext,i)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_import_as_names

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterImport_as_names" ):
                listener.enterImport_as_names(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitImport_as_names" ):
                listener.exitImport_as_names(self)




    def import_as_names(self):

        localctx = MiniPython3Parser.Import_as_namesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_import_as_names)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 252
            self.import_as_name()
            self.state = 257
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,16,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 253
                    self.match(MiniPython3Parser.T__4)
                    self.state = 254
                    self.import_as_name() 
                self.state = 259
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,16,self._ctx)

            self.state = 261
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MiniPython3Parser.T__4:
                self.state = 260
                self.match(MiniPython3Parser.T__4)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Dotted_as_namesContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def dotted_as_name(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniPython3Parser.Dotted_as_nameContext)
            else:
                return self.getTypedRuleContext(MiniPython3Parser.Dotted_as_nameContext,i)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_dotted_as_names

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDotted_as_names" ):
                listener.enterDotted_as_names(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDotted_as_names" ):
                listener.exitDotted_as_names(self)




    def dotted_as_names(self):

        localctx = MiniPython3Parser.Dotted_as_namesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_dotted_as_names)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 263
            self.dotted_as_name()
            self.state = 268
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MiniPython3Parser.T__4:
                self.state = 264
                self.match(MiniPython3Parser.T__4)
                self.state = 265
                self.dotted_as_name()
                self.state = 270
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Dotted_nameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self, i:int=None):
            if i is None:
                return self.getTokens(MiniPython3Parser.NAME)
            else:
                return self.getToken(MiniPython3Parser.NAME, i)

        def getRuleIndex(self):
            return MiniPython3Parser.RULE_dotted_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDotted_name" ):
                listener.enterDotted_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDotted_name" ):
                listener.exitDotted_name(self)




    def dotted_name(self):

        localctx = MiniPython3Parser.Dotted_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_dotted_name)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 271
            self.match(MiniPython3Parser.NAME)
            self.state = 276
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MiniPython3Parser.T__30:
                self.state = 272
                self.match(MiniPython3Parser.T__30)
                self.state = 273
                self.match(MiniPython3Parser.NAME)
                self.state = 278
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expr_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(MiniPython3Parser.ExprContext,0)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_expr_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpr_stmt" ):
                listener.enterExpr_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpr_stmt" ):
                listener.exitExpr_stmt(self)




    def expr_stmt(self):

        localctx = MiniPython3Parser.Expr_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_expr_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 279
            self.expr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_expr

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class CompareContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MiniPython3Parser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniPython3Parser.ExprContext)
            else:
                return self.getTypedRuleContext(MiniPython3Parser.ExprContext,i)

        def compoperator(self):
            return self.getTypedRuleContext(MiniPython3Parser.CompoperatorContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCompare" ):
                listener.enterCompare(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCompare" ):
                listener.exitCompare(self)


    class Atom_choiceContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MiniPython3Parser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def atom(self):
            return self.getTypedRuleContext(MiniPython3Parser.AtomContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAtom_choice" ):
                listener.enterAtom_choice(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAtom_choice" ):
                listener.exitAtom_choice(self)


    class BinaryContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MiniPython3Parser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniPython3Parser.ExprContext)
            else:
                return self.getTypedRuleContext(MiniPython3Parser.ExprContext,i)

        def poweroperator(self):
            return self.getTypedRuleContext(MiniPython3Parser.PoweroperatorContext,0)

        def multoperator(self):
            return self.getTypedRuleContext(MiniPython3Parser.MultoperatorContext,0)

        def plusoperator(self):
            return self.getTypedRuleContext(MiniPython3Parser.PlusoperatorContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBinary" ):
                listener.enterBinary(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBinary" ):
                listener.exitBinary(self)


    class UnaryContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MiniPython3Parser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def unaryoperator(self):
            return self.getTypedRuleContext(MiniPython3Parser.UnaryoperatorContext,0)

        def expr(self):
            return self.getTypedRuleContext(MiniPython3Parser.ExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnary" ):
                listener.enterUnary(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnary" ):
                listener.exitUnary(self)



    def expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = MiniPython3Parser.ExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 56
        self.enterRecursionRule(localctx, 56, self.RULE_expr, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 286
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MiniPython3Parser.T__1, MiniPython3Parser.T__47, MiniPython3Parser.T__49, MiniPython3Parser.T__50, MiniPython3Parser.T__51, MiniPython3Parser.STRING, MiniPython3Parser.NUMBER, MiniPython3Parser.NAME]:
                localctx = MiniPython3Parser.Atom_choiceContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 282
                self.atom(0)
                pass
            elif token in [MiniPython3Parser.T__35, MiniPython3Parser.T__36, MiniPython3Parser.T__45]:
                localctx = MiniPython3Parser.UnaryContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 283
                self.unaryoperator()
                self.state = 284
                self.expr(2)
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 306
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,22,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 304
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,21,self._ctx)
                    if la_ == 1:
                        localctx = MiniPython3Parser.BinaryContext(self, MiniPython3Parser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 288
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 289
                        self.poweroperator()
                        self.state = 290
                        self.expr(5)
                        pass

                    elif la_ == 2:
                        localctx = MiniPython3Parser.BinaryContext(self, MiniPython3Parser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 292
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 293
                        self.multoperator()
                        self.state = 294
                        self.expr(5)
                        pass

                    elif la_ == 3:
                        localctx = MiniPython3Parser.BinaryContext(self, MiniPython3Parser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 296
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 297
                        self.plusoperator()
                        self.state = 298
                        self.expr(4)
                        pass

                    elif la_ == 4:
                        localctx = MiniPython3Parser.CompareContext(self, MiniPython3Parser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 300
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 301
                        self.compoperator()
                        self.state = 302
                        self.expr(2)
                        pass

             
                self.state = 308
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,22,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class PoweroperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_poweroperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPoweroperator" ):
                listener.enterPoweroperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPoweroperator" ):
                listener.exitPoweroperator(self)




    def poweroperator(self):

        localctx = MiniPython3Parser.PoweroperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 58, self.RULE_poweroperator)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 309
            self.match(MiniPython3Parser.T__34)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PlusoperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_plusoperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPlusoperator" ):
                listener.enterPlusoperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPlusoperator" ):
                listener.exitPlusoperator(self)




    def plusoperator(self):

        localctx = MiniPython3Parser.PlusoperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 60, self.RULE_plusoperator)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 311
            _la = self._input.LA(1)
            if not(_la==MiniPython3Parser.T__35 or _la==MiniPython3Parser.T__36):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MultoperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_multoperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMultoperator" ):
                listener.enterMultoperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMultoperator" ):
                listener.exitMultoperator(self)




    def multoperator(self):

        localctx = MiniPython3Parser.MultoperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 62, self.RULE_multoperator)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 313
            _la = self._input.LA(1)
            if not(_la==MiniPython3Parser.T__32 or _la==MiniPython3Parser.T__37):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CompoperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_compoperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCompoperator" ):
                listener.enterCompoperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCompoperator" ):
                listener.exitCompoperator(self)




    def compoperator(self):

        localctx = MiniPython3Parser.CompoperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 64, self.RULE_compoperator)
        try:
            self.state = 328
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,23,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 315
                self.match(MiniPython3Parser.T__38)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 316
                self.match(MiniPython3Parser.T__39)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 317
                self.match(MiniPython3Parser.T__40)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 318
                self.match(MiniPython3Parser.T__41)
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 319
                self.match(MiniPython3Parser.T__42)
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 320
                self.match(MiniPython3Parser.T__43)
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 321
                self.match(MiniPython3Parser.T__44)
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 322
                self.match(MiniPython3Parser.T__21)
                pass

            elif la_ == 9:
                self.enterOuterAlt(localctx, 9)
                self.state = 323
                self.match(MiniPython3Parser.T__45)
                self.state = 324
                self.match(MiniPython3Parser.T__21)
                pass

            elif la_ == 10:
                self.enterOuterAlt(localctx, 10)
                self.state = 325
                self.match(MiniPython3Parser.T__46)
                pass

            elif la_ == 11:
                self.enterOuterAlt(localctx, 11)
                self.state = 326
                self.match(MiniPython3Parser.T__46)
                self.state = 327
                self.match(MiniPython3Parser.T__45)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class UnaryoperatorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_unaryoperator

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnaryoperator" ):
                listener.enterUnaryoperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnaryoperator" ):
                listener.exitUnaryoperator(self)




    def unaryoperator(self):

        localctx = MiniPython3Parser.UnaryoperatorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 66, self.RULE_unaryoperator)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 330
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MiniPython3Parser.T__35) | (1 << MiniPython3Parser.T__36) | (1 << MiniPython3Parser.T__45))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AtomContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_atom

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class String_choiceContext(AtomContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MiniPython3Parser.AtomContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def string(self):
            return self.getTypedRuleContext(MiniPython3Parser.StringContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterString_choice" ):
                listener.enterString_choice(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitString_choice" ):
                listener.exitString_choice(self)


    class CallContext(AtomContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MiniPython3Parser.AtomContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def atom(self):
            return self.getTypedRuleContext(MiniPython3Parser.AtomContext,0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniPython3Parser.ExprContext)
            else:
                return self.getTypedRuleContext(MiniPython3Parser.ExprContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCall" ):
                listener.enterCall(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCall" ):
                listener.exitCall(self)


    class Tuple_choiceContext(AtomContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MiniPython3Parser.AtomContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def tuple_expr(self):
            return self.getTypedRuleContext(MiniPython3Parser.Tuple_exprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTuple_choice" ):
                listener.enterTuple_choice(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTuple_choice" ):
                listener.exitTuple_choice(self)


    class Const_choiceContext(AtomContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MiniPython3Parser.AtomContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def name_const(self):
            return self.getTypedRuleContext(MiniPython3Parser.Name_constContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConst_choice" ):
                listener.enterConst_choice(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConst_choice" ):
                listener.exitConst_choice(self)


    class SubscriptContext(AtomContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MiniPython3Parser.AtomContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def atom(self):
            return self.getTypedRuleContext(MiniPython3Parser.AtomContext,0)

        def slice_expr(self):
            return self.getTypedRuleContext(MiniPython3Parser.Slice_exprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSubscript" ):
                listener.enterSubscript(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSubscript" ):
                listener.exitSubscript(self)


    class Num_choiceContext(AtomContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MiniPython3Parser.AtomContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def num(self):
            return self.getTypedRuleContext(MiniPython3Parser.NumContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNum_choice" ):
                listener.enterNum_choice(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNum_choice" ):
                listener.exitNum_choice(self)


    class Name_choiceContext(AtomContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MiniPython3Parser.AtomContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def name(self):
            return self.getTypedRuleContext(MiniPython3Parser.NameContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterName_choice" ):
                listener.enterName_choice(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitName_choice" ):
                listener.exitName_choice(self)


    class AttributeContext(AtomContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MiniPython3Parser.AtomContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def atom(self):
            return self.getTypedRuleContext(MiniPython3Parser.AtomContext,0)

        def NAME(self):
            return self.getToken(MiniPython3Parser.NAME, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAttribute" ):
                listener.enterAttribute(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAttribute" ):
                listener.exitAttribute(self)


    class List_choiceContext(AtomContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MiniPython3Parser.AtomContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def list_expr(self):
            return self.getTypedRuleContext(MiniPython3Parser.List_exprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterList_choice" ):
                listener.enterList_choice(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitList_choice" ):
                listener.exitList_choice(self)


    class BracketedContext(AtomContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a MiniPython3Parser.AtomContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(MiniPython3Parser.ExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBracketed" ):
                listener.enterBracketed(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBracketed" ):
                listener.exitBracketed(self)



    def atom(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = MiniPython3Parser.AtomContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 68
        self.enterRecursionRule(localctx, 68, self.RULE_atom, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 343
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,24,self._ctx)
            if la_ == 1:
                localctx = MiniPython3Parser.Num_choiceContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 333
                self.num()
                pass

            elif la_ == 2:
                localctx = MiniPython3Parser.Name_choiceContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 334
                self.name()
                pass

            elif la_ == 3:
                localctx = MiniPython3Parser.String_choiceContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 335
                self.string()
                pass

            elif la_ == 4:
                localctx = MiniPython3Parser.List_choiceContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 336
                self.list_expr()
                pass

            elif la_ == 5:
                localctx = MiniPython3Parser.Tuple_choiceContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 337
                self.tuple_expr()
                pass

            elif la_ == 6:
                localctx = MiniPython3Parser.Const_choiceContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 338
                self.name_const()
                pass

            elif la_ == 7:
                localctx = MiniPython3Parser.BracketedContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 339
                self.match(MiniPython3Parser.T__1)
                self.state = 340
                self.expr(0)
                self.state = 341
                self.match(MiniPython3Parser.T__2)
                pass


            self._ctx.stop = self._input.LT(-1)
            self.state = 368
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,28,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 366
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,27,self._ctx)
                    if la_ == 1:
                        localctx = MiniPython3Parser.CallContext(self, MiniPython3Parser.AtomContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_atom)
                        self.state = 345
                        if not self.precpred(self._ctx, 10):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 10)")
                        self.state = 346
                        self.match(MiniPython3Parser.T__1)
                        self.state = 348
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MiniPython3Parser.T__1) | (1 << MiniPython3Parser.T__35) | (1 << MiniPython3Parser.T__36) | (1 << MiniPython3Parser.T__45) | (1 << MiniPython3Parser.T__47) | (1 << MiniPython3Parser.T__49) | (1 << MiniPython3Parser.T__50) | (1 << MiniPython3Parser.T__51) | (1 << MiniPython3Parser.STRING) | (1 << MiniPython3Parser.NUMBER) | (1 << MiniPython3Parser.NAME))) != 0):
                            self.state = 347
                            self.expr(0)


                        self.state = 354
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        while _la==MiniPython3Parser.T__4:
                            self.state = 350
                            self.match(MiniPython3Parser.T__4)
                            self.state = 351
                            self.expr(0)
                            self.state = 356
                            self._errHandler.sync(self)
                            _la = self._input.LA(1)

                        self.state = 357
                        self.match(MiniPython3Parser.T__2)
                        pass

                    elif la_ == 2:
                        localctx = MiniPython3Parser.AttributeContext(self, MiniPython3Parser.AtomContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_atom)
                        self.state = 358
                        if not self.precpred(self._ctx, 9):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 9)")
                        self.state = 359
                        self.match(MiniPython3Parser.T__30)
                        self.state = 360
                        self.match(MiniPython3Parser.NAME)
                        pass

                    elif la_ == 3:
                        localctx = MiniPython3Parser.SubscriptContext(self, MiniPython3Parser.AtomContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_atom)
                        self.state = 361
                        if not self.precpred(self._ctx, 8):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 8)")
                        self.state = 362
                        self.match(MiniPython3Parser.T__47)
                        self.state = 363
                        self.slice_expr()
                        self.state = 364
                        self.match(MiniPython3Parser.T__48)
                        pass

             
                self.state = 370
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,28,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Slice_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def subscript_index(self):
            return self.getTypedRuleContext(MiniPython3Parser.Subscript_indexContext,0)


        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniPython3Parser.ExprContext)
            else:
                return self.getTypedRuleContext(MiniPython3Parser.ExprContext,i)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_slice_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSlice_expr" ):
                listener.enterSlice_expr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSlice_expr" ):
                listener.exitSlice_expr(self)




    def slice_expr(self):

        localctx = MiniPython3Parser.Slice_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 70, self.RULE_slice_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 376
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,29,self._ctx)
            if la_ == 1:
                self.state = 371
                self.subscript_index()
                pass

            elif la_ == 2:
                self.state = 372
                self.expr(0)
                self.state = 373
                self.match(MiniPython3Parser.T__3)
                self.state = 374
                self.expr(0)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Subscript_indexContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(MiniPython3Parser.ExprContext,0)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_subscript_index

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSubscript_index" ):
                listener.enterSubscript_index(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSubscript_index" ):
                listener.exitSubscript_index(self)




    def subscript_index(self):

        localctx = MiniPython3Parser.Subscript_indexContext(self, self._ctx, self.state)
        self.enterRule(localctx, 72, self.RULE_subscript_index)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 378
            self.expr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Name_constContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_name_const

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterName_const" ):
                listener.enterName_const(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitName_const" ):
                listener.exitName_const(self)




    def name_const(self):

        localctx = MiniPython3Parser.Name_constContext(self, self._ctx, self.state)
        self.enterRule(localctx, 74, self.RULE_name_const)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 380
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MiniPython3Parser.T__49) | (1 << MiniPython3Parser.T__50) | (1 << MiniPython3Parser.T__51))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NumContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self):
            return self.getToken(MiniPython3Parser.NUMBER, 0)

        def getRuleIndex(self):
            return MiniPython3Parser.RULE_num

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNum" ):
                listener.enterNum(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNum" ):
                listener.exitNum(self)




    def num(self):

        localctx = MiniPython3Parser.NumContext(self, self._ctx, self.state)
        self.enterRule(localctx, 76, self.RULE_num)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 382
            self.match(MiniPython3Parser.NUMBER)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(MiniPython3Parser.NAME, 0)

        def getRuleIndex(self):
            return MiniPython3Parser.RULE_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterName" ):
                listener.enterName(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitName" ):
                listener.exitName(self)




    def name(self):

        localctx = MiniPython3Parser.NameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 78, self.RULE_name)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 384
            self.match(MiniPython3Parser.NAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StringContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRING(self):
            return self.getToken(MiniPython3Parser.STRING, 0)

        def getRuleIndex(self):
            return MiniPython3Parser.RULE_string

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterString" ):
                listener.enterString(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitString" ):
                listener.exitString(self)




    def string(self):

        localctx = MiniPython3Parser.StringContext(self, self._ctx, self.state)
        self.enterRule(localctx, 80, self.RULE_string)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 386
            self.match(MiniPython3Parser.STRING)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class List_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniPython3Parser.ExprContext)
            else:
                return self.getTypedRuleContext(MiniPython3Parser.ExprContext,i)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_list_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterList_expr" ):
                listener.enterList_expr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitList_expr" ):
                listener.exitList_expr(self)




    def list_expr(self):

        localctx = MiniPython3Parser.List_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 82, self.RULE_list_expr)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 388
            self.match(MiniPython3Parser.T__47)
            self.state = 390
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MiniPython3Parser.T__1) | (1 << MiniPython3Parser.T__35) | (1 << MiniPython3Parser.T__36) | (1 << MiniPython3Parser.T__45) | (1 << MiniPython3Parser.T__47) | (1 << MiniPython3Parser.T__49) | (1 << MiniPython3Parser.T__50) | (1 << MiniPython3Parser.T__51) | (1 << MiniPython3Parser.STRING) | (1 << MiniPython3Parser.NUMBER) | (1 << MiniPython3Parser.NAME))) != 0):
                self.state = 389
                self.expr(0)


            self.state = 396
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,31,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 392
                    self.match(MiniPython3Parser.T__4)
                    self.state = 393
                    self.expr(0) 
                self.state = 398
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,31,self._ctx)

            self.state = 400
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MiniPython3Parser.T__4:
                self.state = 399
                self.match(MiniPython3Parser.T__4)


            self.state = 402
            self.match(MiniPython3Parser.T__48)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Tuple_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniPython3Parser.ExprContext)
            else:
                return self.getTypedRuleContext(MiniPython3Parser.ExprContext,i)


        def getRuleIndex(self):
            return MiniPython3Parser.RULE_tuple_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTuple_expr" ):
                listener.enterTuple_expr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTuple_expr" ):
                listener.exitTuple_expr(self)




    def tuple_expr(self):

        localctx = MiniPython3Parser.Tuple_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 84, self.RULE_tuple_expr)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 404
            self.match(MiniPython3Parser.T__1)

            self.state = 405
            self.expr(0)
            self.state = 408 
            self._errHandler.sync(self)
            _alt = 1
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt == 1:
                    self.state = 406
                    self.match(MiniPython3Parser.T__4)
                    self.state = 407
                    self.expr(0)

                else:
                    raise NoViableAltException(self)
                self.state = 410 
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,33,self._ctx)

            self.state = 413
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MiniPython3Parser.T__4:
                self.state = 412
                self.match(MiniPython3Parser.T__4)


            self.state = 415
            self.match(MiniPython3Parser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[28] = self.expr_sempred
        self._predicates[34] = self.atom_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx:ExprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 5)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 4)
         

            if predIndex == 2:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 1)
         

    def atom_sempred(self, localctx:AtomContext, predIndex:int):
            if predIndex == 4:
                return self.precpred(self._ctx, 10)
         

            if predIndex == 5:
                return self.precpred(self._ctx, 9)
         

            if predIndex == 6:
                return self.precpred(self._ctx, 8)
         




