"""
Implements utility functions to parse Python programs as
syntax trees and load data from this set.

Copyright (C) 2020
Benjamin Paaßen
Machine Learning Research Group
Bielefeld University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '1.0.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

import ast
import logging
import json
import os

_tasks = ['fun', 'plt', 'grad', 'desc', 'fin']

def load_student(filepath):
    """ Loads the data from a single student for all tasks. The result
    is a list of tasks, where each task contains a list of all programs
    the student had while solving the task.
    Each program  in turn is a triple (nodes, adj, var), where nodes
    is a list of syntactic nodes in the program, adj is an adjacency list
    and var is a map of variable names to nodes where this variable was used.

    Args:
    filepath: The path to a json file containing this students data.

    Returns:
    seqs: A list of program sequences, one per task. Each sequence is a
          list of trees, where each tree in turn is a triple of a node list,
          an adjacency list, and a variable map as described above.
    """
    # initialize empty sequence list
    seqs = []
    with open(filepath, 'r') as json_file:
        # parse the json file
        json_data = json.load(json_file)
        # iterate over all tasks
        for task in _tasks:
            seq = []
            # extract only the data for the current task
            src_list  = json_data[task]
            # iterate over the source code list
            for t in range(len(src_list)):
                src = src_list[t]
                # try to parse the source code. If it fails, produce a warning
                try:
                    src_ast = ast.parse(src)
                except SyntaxError as ex:
                    logging.warning('The source code for task %s (%dth step) was invalid: %s' % (task, t + 1, src))
                    logging.warning(str(ex))
                    continue
                # convert the abstract syntax tree to our tree format
                # and attach it to the sequence
                seq.append(ast_to_tree(src_ast))
            # append the sequence to the sequence list
            seqs.append(seq)
    return seqs

def load_task(identifier, data_dir = 'data_cleaned'):
    """ Loads the data from all students for the given task identifier as a
    list of tree sequences. In more detail, the result is a list seqs where
    seqs[i] contains all programs of the ith student while solving the given
    task.
    Each program seqs[i][t] in turn is a triple (nodes, adj, var), where nodes
    is a list of syntactic nodes in the program, adj is an adjacency list
    and var is a map of variable names to nodes where this variable was used.

    So, for example, the Python code "i = 3" would correspond to the tree
    nodes = ['Assign', 'Name', 'Num'], adj = [[1, 2], [], []], and
    var = {'i': [1]}.

    Args:
    identifier: A task identifier. Can be either a task number (1-5),
                or one of 'fun', 'plt', 'grad', 'desc', or 'fin', corresponding
                to the function definition task, the plotting task, the
                gradient definition task, the actual gradient descent
                implementation task, and the final task, where students had to
                use their implementation to optimize the given function.
    data_dir:   The path to the data directory. Defaults to 'data_cleaned'.

    Returns:
    seqs: A list of program sequences, one per student. Each sequence is a
          list of trees, where each tree in turn is a triple of a node list,
          an adjacency list, and a variable map as described above.
    """
    if(not os.path.isdir(data_dir)):
        raise OSError('%s is not a directory or was not found' % str(data_dir))
    if isinstance(identifier, int):
        if identifier < 1 or identifier > 5:
            raise ValueError('Invalid task identifier \"%s\"; must be 1-5 or one of fun, plt, grad, desc, fin.' % str(identifier))
        identifier = _tasks[identifier-1]
    elif identifier not in _tasks:
        raise ValueError('Invalid task identifier \"%s\"; must be 1-5 or one of fun, plt, grad, desc, fin.' % str(identifier))
    
    # list all files in the directory and sort them.
    files = os.listdir(data_dir)
    files.sort()

    # initialize an empty output list.
    seqs = []
    # iterate over all files and parse them
    for filename in files:
        seq = []
        # note that 'listdir' returns files relative to the input path, so
        # we have to put the input path in front.
        filepath = data_dir + '/' + filename
        # check if the file is a json file.
        if(not os.path.isfile(filepath) or not filename.endswith('.json')):
            continue
        with open(filepath, 'r') as json_file:
            # load the json file
            json_data = json.load(json_file)
            # extract only the data for the current task
            src_list  = json_data[identifier]
            # iterate over the source code list
            for t in range(len(src_list)):
                src = src_list[t]
                # try to path the source code. If it fails, produce a warning
                try:
                    src_ast = ast.parse(src)
                except SyntaxError as ex:
                    logging.warning('The source code for task %s in file %s (%dth step) was invalid: %s' % (identifier, filename, t + 1, src))
                    logging.warning(str(ex))
                    continue
                # convert the abstract syntax tree to our tree format
                # and attach it to the sequence
                seq.append(ast_to_tree(src_ast))
        # append the sequence to the sequence list
        seqs.append(seq)
    return seqs


def ast_to_tree(ast):
    """ Converts a Python abstract syntax tree as returned by the Python
    compiler into a node list/adjacency list format.

    Args:
    ast: An abstract syntax tree in Pythons internal compiler format.

    Returns:
    nodes: A node list, where each element is a string identifying a syntactic
           element of the input program (e.g. 'If', 'For', 'expr', etc.).
    adj:   An adjacency list defining the tree structure.
    var:   A map of variable names to a list of node indices where this
           variable is used in the program.
    """
    nodes = []
    adj   = []
    var   = {}
    # go through the AST via depth first search and accumulate nodes,
    # adjacencies, and variables
    _ast_to_tree(ast, nodes, adj, var)
    return nodes, adj, var

def _ast_to_tree(ast_node, nodes, adj, var):
    # append the current node to the tree
    i = len(nodes)
    symbol = ast_node.__class__.__name__
    nodes.append(symbol)
    adj_i = []
    adj.append(adj_i)
    # check if this node has an 'arg' or an 'id' attribute, in which
    # case we need to store a new variable reference
    varname = None
    if(hasattr(ast_node, 'arg')):
        varname = ast_node.arg
    if(symbol == 'Name' and hasattr(ast_node, 'id')):
        varname = ast_node.id
    if(hasattr(ast_node, 'name')):
        varname = ast_node.name
    if(symbol == 'Attribute' and hasattr(ast_node, 'attr')):
        varname = ast_node.attr
    if symbol == 'Num':
        varname = ast_node.n
    if(varname is not None):
        reflist = var.setdefault(varname, [])
        reflist.append(i)
    if(symbol == 'If'):
        # explicitly treat if nodes differently because they include
        # two statement lists, which we need to disambiguate
        adj_i.append(len(nodes))
        _ast_to_tree(ast_node.test, nodes, adj, var)
        adj_i.append(len(nodes))
        nodes.append('Then')
        adj_j = []
        adj.append(adj_j)
        for node in ast_node.body:
            adj_j.append(len(nodes))
            _ast_to_tree(node, nodes, adj, var)
        adj_i.append(len(nodes))
        nodes.append('Else')
        adj_j = []
        adj.append(adj_j)
        for node in ast_node.orelse:
            adj_j.append(len(nodes))
            _ast_to_tree(node, nodes, adj, var)
        return
    # handle all children of this node recursively
    for node in ast.iter_child_nodes(ast_node):
        # ignore some nodes
        if(isinstance(node, ast.Load) or isinstance(node, ast.Store)):
            continue
        adj_i.append(len(nodes))
        _ast_to_tree(node, nodes, adj, var)
