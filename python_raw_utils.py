"""
Implements utility functions to parse Python programs as
syntax trees and load data from this set.

Copyright (C) 2020
Benjamin Paaßen
Machine Learning Research Group
Bielefeld University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '1.0.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

import logging
import json
import os
from antlr4 import InputStream
from antlr4 import CommonTokenStream
from antlr4.tree.Tree import TerminalNode
from antlr4.tree.Tree import ErrorNode
from antlr.MiniPython3Lexer import MiniPython3Lexer
from antlr.MiniPython3Parser import MiniPython3Parser

import edist.tree_utils as te

_tasks = ['fun', 'plt', 'grad', 'desc', 'fin']

def load_student(filepath):
    """ Loads the data from a single student for all tasks. The result
    is a list of tasks, where each task contains a list of all programs
    the student had while solving the task.
    Each program  in turn is a triple (nodes, adj, var), where nodes
    is a list of syntactic nodes in the program, adj is an adjacency list
    and var is a map of variable names to nodes where this variable was used.

    Args:
    filepath: The path to a json file containing this students data.

    Returns:
    seqs: A list of program sequences, one per task. Each sequence is a
          list of trees, where each tree in turn is a triple of a node list,
          an adjacency list, and a variable map as described above.
    """
    # initialize empty sequence list
    seqs = []
    with open(filepath, 'r') as json_file:
        # parse the json file
        json_data = json.load(json_file)
        # iterate over all tasks
        for task in _tasks:
            seq = []
            # extract only the data for the current task
            src_list  = json_data[task]
            # iterate over the source code list
            for t in range(len(src_list)):
                src = src_list[t]
                # parse the source code using the antlr4 parser
                ast = parse_src_antlr4(src)
                seq.append(ast)
            # append the sequence to the sequence list
            seqs.append(seq)
    return seqs

def load_task(identifier, data_dir = 'data_cleaned'):
    """ Loads the data from all students for the given task identifier as a
    list of tree sequences. In more detail, the result is a list seqs where
    seqs[i] contains all programs of the ith student while solving the given
    task.
    Each program seqs[i][t] in turn is a triple (nodes, adj, var), where nodes
    is a list of syntactic nodes in the program, adj is an adjacency list
    and var is a map of variable names to nodes where this variable was used.

    So, for example, the Python code "i = 3" would correspond to the tree
    nodes = ['Assign', 'Name', 'Num'], adj = [[1, 2], [], []], and
    var = {'i': [1]}.

    Args:
    identifier: A task identifier. Can be either a task number (1-5),
                or one of 'fun', 'plt', 'grad', 'desc', or 'fin', corresponding
                to the function definition task, the plotting task, the
                gradient definition task, the actual gradient descent
                implementation task, and the final task, where students had to
                use their implementation to optimize the given function.
    data_dir:   The path to the data directory. Defaults to 'data_cleaned'.

    Returns:
    seqs: A list of program sequences, one per student. Each sequence is a
          list of trees, where each tree in turn is a triple of a node list,
          an adjacency list, and a variable map as described above.
    """
    if(not os.path.isdir(data_dir)):
        raise OSError('%s is not a directory or was not found' % str(data_dir))
    if isinstance(identifier, int):
        if identifier < 1 or identifier > 5:
            raise ValueError('Invalid task identifier \"%s\"; must be 1-5 or one of fun, plt, grad, desc, fin.' % str(identifier))
        identifier = _tasks[identifier-1]
    elif identifier not in _tasks:
        raise ValueError('Invalid task identifier \"%s\"; must be 1-5 or one of fun, plt, grad, desc, fin.' % str(identifier))
    
    # list all files in the directory and sort them.
    files = os.listdir(data_dir)
    files.sort()

    # initialize an empty output list.
    seqs = []
    # iterate over all files and parse them
    for filename in files:
        seq = []
        # note that 'listdir' returns files relative to the input path, so
        # we have to put the input path in front.
        filepath = data_dir + '/' + filename
        # check if the file is a json file.
        if(not os.path.isfile(filepath) or not filename.endswith('.json')):
            continue
        with open(filepath, 'r') as json_file:
            # load the json file
            json_data = json.load(json_file)
            # extract only the data for the current task
            src_list  = json_data[identifier]
            # iterate over the source code list
            for t in range(len(src_list)):
                src = src_list[t]
                # parse the source code using the antlr4 parser
                ast = parse_src_antlr4(src)
                seq.append(ast)
        # append the sequence to the sequence list
        seqs.append(seq)
    return seqs

def parse_src_antlr4(src):
    """ Parses a given snippet of python source code via the antlr4
    MiniPython parser and returns it in node/adjacency list format.

    Args:
    src: a string of Python3 source code.

    Returns:
    nodes: A node list, where each element is a string identifying a syntactic
           element of the input program (e.g. 'If', 'For', 'expr', etc.).
    adj:   An adjacency list defining the tree structure.
    var:   A map of variable names to a list of node indices where this
           variable is used in the program.
    """
    # setup parsing class instances
    inp = InputStream(src)
    lexer = MiniPython3Lexer(inp)
    stream = CommonTokenStream(lexer)
    parser = MiniPython3Parser(stream)
    # perform the parsing process
    tree = parser.module()

    # walk through the tree and convert it to node list/adjacency list format
    nodes = []
    adj   = []
    var   = {}
    _convert_tree(tree, nodes, adj, var)

    return nodes, adj, var

translations = {
    'module' : 'Module',
    'funcdef' : 'FunctionDef',
    'argument_list' : 'arguments',
    'arg' : 'arg',
    'return_stmt' : 'Return',
    'assign_stmt' : 'Assign',
    'augassign_stmt' : 'AugAssign',
    'for_stmt' : 'For',
    'while_stmt' : 'While',
    'if_stmt' : 'If',
    'if_else_stmt' : 'IfElse',
    'then_stmt' : 'Then',
    'else_stmt' : 'Else',
    'import_stmt' : 'Import',
    'pass_stmt' : 'Pass',
    'break_stmt' : 'Break',
    'continue_stmt' : 'Continue',
    'expr_stmt' : 'expression',
    'then_stmt' : 'Then',
    'else_stmt' : 'Else',
    'binary' : 'BinOp',
    'compare' : 'Compare',
    'unary' : 'UnaryOp',
    'call' : 'Call',
    'attribute' : 'Attribute',
    'subscript' : 'Subscript',
    'num' : 'Num',
    'name' : 'Name',
    'name_const' : 'NameConstant',
    'string' : 'Str',
    'list_expr' : 'List',
    'tuple_expr' : 'Tuple',
    'subscript_index' : 'Index',
    'or' : 'Or',
    'and' : 'And',
    '+' : 'Add',
    '-' : 'Sub',
    '*' : 'Mult',
    '/' : 'Div',
    '%' : 'Mod',
    '**' : 'Pow',
    'not' : 'Not',
    '+=' : 'Add',
    '-=' : 'Sub',
    '*=' : 'Mult',
    '/=' : 'Div',
    '%=' : 'Mod',
    '**=' : 'Pow',
    '=' : 'Eq',
    '!=' : 'NotEq',
    '<' : 'Lt',
    '<=' : 'LtE',
    '>' : 'Gt',
    '>=' : 'GtE',
    'is' : 'Is',
    'is not' : 'IsNot',
    'in' : 'In',
    'not in' : 'NotIn'
}

def _convert_tree(tree, nodes, adj, var, p = -1, i = 0):
    if isinstance(tree, ErrorNode):
        return i
    elif isinstance(tree, TerminalNode):
        return i
    node_name = type(tree).__name__
    node_name = node_name[:len(node_name)-len('Context')].lower()
    if node_name in ['num', 'name', 'string', 'arg']:
        varname = tree.getRuleContext().getText()
        reflist = var.setdefault(varname, [])
        reflist.append(i)
    if node_name == 'attribute':
        children = tree.getChildren()
        children.__next__()
        children.__next__()
        child = children.__next__()
        if not isinstance(child, TerminalNode):
            raise ValueError('Expected terminal node as third child of an attribute')
        varname = child.getText()
        print('attribute name: %s' % varname)
        reflist = var.setdefault(varname, [])
        reflist.append(i)
    if node_name in ['plusoperator', 'multoperator', 'poweroperator', 'compoperator', 'unaryoperator', 'augoperator']:
        opname = tree.getRuleContext().getText()
        if node_name == 'unaryoperator':
            if opname == '-':
                opname = 'USub'
            elif opname == '+':
                opname = 'UAdd'
            else:
                opname = 'Not'
        else:
            opname = translations[opname]
        nodes.append(opname)
        adj.append([])
        adj[p].append(i)
        j = i + 1
    elif node_name == 'funcdef':
        children = tree.getChildren()
        child = children.__next__()
        if not isinstance(child, TerminalNode):
            raise ValueError('Expected terminal node as first child of a function definition')
        child = children.__next__()
        if type(child).__name__ != 'NameContext':
            raise ValueError('Expected name node as second child of a function definition but was %s' % type(child).__name__)
        nodes.append(translations[node_name])
        adj.append([])
        adj[p].append(i)
        j = i + 1
        fun_name = child.getRuleContext().getText()
        reflist = var.setdefault(fun_name, [])
        reflist.append(i)
        for child in children:
            j = _convert_tree(child, nodes, adj, var, i, j)
    elif node_name in translations:
        nodes.append(translations[node_name])
        adj.append([])
        if p >= 0:
            adj[p].append(i)
        j = i + 1
        for child in tree.getChildren():
            j = _convert_tree(child, nodes, adj, var, i, j)
    else:
        j = i
        for child in tree.getChildren():
            j = _convert_tree(child, nodes, adj, var, p, j)
    return j
