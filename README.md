# Python Data Science Programming Study

Copyright (C) 2020 - Benjamin Paassen  
Machine Learning Research Group  
Bielefeld University

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>.

## Introduction

This repository contains programming data collected from 15 students during
November and December of 2019 at Bielefeld University. Students were asked to
implement [gradient descent][gdesc] in five steps, in particular:

1. Implement the reference function $`f(x) = x^4 - x^2 + \frac{x}{4}`$
    in Python (`fun` task).
2. Plot the function in the range \[-1.5, +1.5\] (`plt` task).
3. Implement the gradient $`\frac{\partial}{\partial x} f(x)`$ (`grad` task).
4. Implement the [gradient descent algorithm][gdesc] itself (`desc` task).
5. Apply the gradient descent algorithm to find the global minimum of $`f`$
    (`fin` task).

Students were only allowed to progress to the next step once the previous
step was completed. After all tasks were completed, students could voluntarily
submit their data to the principal investigator. Note that this implies that
only successful solutions are contained in this data set (according to
automated tests).

Most students (13) performed the study with the principal investigator present
as tutor, i.e. the PI could be approached for questions and guidance. Two
students performed the study without guidance at home and send in their
code via e-mail.

All students programmed in a web environment, which you can find under
`python_study.html`. This environment was also used to record the data.
A snapshot of the students current source code was recorded whenever the
student changed the code and then did not apply further changes for at least
two seconds. Note that this data set contains only source code snapshots and
neither timestamps nor personal information. In particular, the folder
 `data_raw` contains the raw source code of all students and the folder
`data_cleaned` containts the code without syntax errors but still indicating
the semantic errors the students made.

If you wish to use this dataset in research, please refer to it under the
DOI [10.4119/unibi/2941052](https://doi.org/10.4119/unibi/2941052).

## Data Format

The data in this dataset is formatted as a [json][json] file for each student.
Each json file has the following structure:

```
{
  "fun": [
    "<First source code state for first task>",
    ...
    "<Last source code state for first task>",
  ],
  ...
  "fin": [
    "<First source code state for last task>",
    ...
    "<Last source code state for last task>",
  ],
```

In other words, each json file is a dictionary with five entries, one for each
task (`fun`, `plt`, `grad`, `desc`, and `fin`). Each task entry contains a list
of source code snapshots recorded for the respective student in the respective
task.

You can find helper functions to parse the data in the files
`python_ast_utils.py` (for the `data_cleaned` folder) and
`python_raw_utils.py` (for the `data_raw` folder).

## Contents

In more detail, this repository contains the following files:

* `ace-small` : A reduced version of the [ace code editor][ace] with only a
    single theme and only Python support.
* `antlr` : A robust Python parser generated via [antlr4](https://www.antlr.org/),
    which can handle the syntax errors in the `data_raw` directory.
* `background.svg` : The University of Bielefeld Logo.
* `data_cleaned` : A version of this data set where all syntax errors and
    repitions where manually removed.
* `data_raw` : The raw data of this study, including repitions and syntax
    errors.
* `ethics_approval.pdf` : The ethics approval for this study.
* `ethics_application.pdf` : The ethics application for this study.
* `LICENSE.md` : The GNU Public License Version 3.
* `pyodide-0.14-small` : A reduced version of the [pyodide][pyo] to run Python
    code from javascript.
* `python_ast_utils.py` : A collection of utility functions to load data
    from the cleaned version of this data set.
* `python_raw_utils.py` : A collection of utility functions to load data
    from the raw version of this data set.
* `python_study.html` : The actual code of the study.
* `README.md` : This file.

## License

All source code in this folder as well as the `antlr`, `data_raw`, and `data_cleaned`
directories is licensed according to the
[GNU General Public License Version 3][GPLv3] (refer to `LICENSE.md`).
We use the [ace code editor][ace] according to the BSD License
(refer to `ace-small/LICENSE`). We use the [pyodide][pyo] project according
to the Mozilla Public License, Version 2 (refer to `pyodide-0.14-small/LICENSE`).
We use [antlr][antlr] according to the BSD License (refer to https://www.antlr.org/license.html) and the antlr Python grammar of Bart Kiers according to
the MIT License.

[gdesc]:https://en.wikipedia.org/wiki/Gradient_descent#Description "Wikipedia Page on Gradient Descent"
[ace]:https://ace.c9.io "Ace Code Editor project page"
[pyo]:https://github.com/iodide-project/pyodide/ "pyodide github page"
[antlr]:https://www.antlr.org "The ANTLR parser generator project"
[json]:https://www.json.org/json-en.html "The JSON data format"
[GPLv3]: https://www.gnu.org/licenses/gpl-3.0.en.html "The GNU General Public License Version 3"
